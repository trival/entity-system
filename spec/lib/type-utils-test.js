var utils = require('type-utils');

describe('Type utils', function() {

  it('has a incrementing id function', function() {
    let id1 = utils.newUid();
    let id2 = utils.newUid();

    expect(id2 > id1).to.be["true"];
  });

  it('can parse name and namespace from a entityString', function() {
    let [name, namespace] = utils.getNameNamespaceFromEntityKey('foo')

    expect(name).to.equal('foo');
    expect(namespace).to.equal('');

    [name, namespace] = utils.getNameNamespaceFromEntityKey('bar/foo')

    expect(name).to.equal('foo');
    expect(namespace).to.equal('bar');
  });
});
