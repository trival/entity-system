var Runtime = require('runtime'),
    Manager = require('spec-manager'),
    Types = require('spec-types')


describe('System Specification Manager', function() {

  var manager

  beforeEach(function() {
    manager = Manager.create()
  })


  it('can create new specs', function() {
    expect(Manager.createSpec()).to.deep.equal({
      entities: {},
      factories: {},
      reactions: {},
      oscillators: {},
      metadata: {}
    })
  })


  describe('Spec transforms', function() {

    it("has a fresh new spec after creation", function() {
      expect(manager.getSpec()).to.deep.equal(Manager.createSpec())
    })


    describe("addEntity", function() {

      it('can add entities in a functional way', function() {
        let e1 = Types.createEntity({
          name: 'foo'
        })
        let e2 = Types.createEntity({
          name: 'kuku',
          namespace: 'lala'
        })

        let spec1 = manager.getSpec(),
            result1 = manager.addEntity(e1),
            spec2 = manager.getSpec(),
            result2 = manager.addEntity(e2),
            spec3 = manager.getSpec()

        expect(spec3.entities).to.deep.equal({
          [result1.id]: result1,
          [result2.id]: result2
        })

        expect(spec1).to.deep.equal(Manager.createSpec())

        expect(spec2).to.not.deep.equal(spec1)
        expect(spec3).to.not.deep.equal(spec2)
      })


      it('can add a bulk of entities', function() {
        manager.addEntities([
          {
            name: 'foo'
          }, {
            name: 'bar',
            namespace: 'kuku'
          }, {
            name: 'fufu'
          }
        ])

        expect(manager.getEntityByNameNamespace('foo')).to.exist
        expect(manager.getEntityByNameNamespace('bar', 'kuku')).to.exist
        expect(manager.getEntityByNameNamespace('fufu')).to.exist
      })
    })


    describe('addFactory', function() {

      it('can add factories in a functional way', function() {
        let e = manager.addEntity({
          name: 'foo'
        })

        let data = {
          receiver: e.id,
          procedure: function() {}
        }

        let oldSpec = manager.getSpec(),
            factory = manager.addFactory(data),
            newSpec = manager.getSpec()

        expect(newSpec.factories).to.deep.equal({
          [factory.id]: factory
        })

        expect(oldSpec.factories).to.deep.equal({})
      })


      it('does not allow adding factories without an existing receiver', function() {
        let data = {
          receiver: 'foo',
          procedure: function() {}
        }

        function test () {
          manager.addFactory(data)
        }

        expect(test).to["throw"](/receiver.*foo/)
      })


      it('adds the factory id to the receiver entity', function() {
        let eid = 'foo'
        let name = 'lululu'
        let oldEntity = manager.addEntity({
          id: eid,
          name: name
        })

        let factory = manager.addFactory({
          receiver: eid,
          procedure: function() {}
        })

        let entity = manager.getSpec().entities[eid]

        expect(entity.factory).to.equal(factory.id)
        expect(oldEntity.factory).to.not.exist
      })
    })


    describe('addReaction', function() {

      it('can add reactions in a functional way', function() {
        let e = manager.addEntity({
          name: 'foo'
        })

        let data = {
          receiver: e.id,
          procedure: function(foo, bubu) {},
          triggers: ['bubu']
        }

        let oldSpec = manager.getSpec()
        let reaction = manager.addReaction(data)
        let newSpec = manager.getSpec()

        expect(newSpec.reactions).to.deep.equal({
          [reaction.id]: reaction,
        })

        expect(oldSpec.reactions).to.deep.equal({})
      })


      it('does not allow adding reactions without an existing receiver', function() {
        let data = {
          receiver: 'foo',
          procedure: function(foo, fuu) {},
          triggers: ['fuu']
        }

        function test () {
          manager.addReaction(data)
        }

        expect(test).to["throw"](/receiver.*foo/)
      })


      it('adds reaction to the entity reactions set', function() {
        let eid = 'foo'

        manager.addEntity({
          id: eid,
          name: eid
        })

        let r1 = Types.createReaction({
          receiver: eid,
          procedure: function(e, fuu) {},
          triggers: ['fuu']
        })

        let r2 = Types.createReaction({
          receiver: eid,
          procedure: function(e, bar) {},
          triggers: ['bar']
        })

        expect(manager.getSpec().entities[eid].reactions).to.not.exist

        manager.addReaction(r1)

        expect(manager.getSpec().entities[eid].reactions).to.deep.equal([r1.id])

        manager.addReaction(r2)

        expect(manager.getSpec().entities[eid].reactions).to.deep.equal([r1.id, r2.id])

        manager.addReaction(r1)

        expect(manager.getSpec().entities[eid].reactions).to.deep.equal([r1.id, r2.id])
      })
    })


    describe('addOscillator', function() {

      it('can add oscillators in a functional way', function() {
        let e = manager.addEntity({
          name: 'foo'
        })

        let t = manager.addEntity({
          name: 'bar'
        })

        let data = {
          receiver: e.id,
          target: t.id,
          procedure: function() {}
        }

        let oldSpec = manager.getSpec()
        let oscillator = manager.addOscillator(data)
        let newSpec = manager.getSpec()

        expect(newSpec.oscillators).to.deep.equal({
          [oscillator.id]: oscillator,
        })

        expect(oldSpec.oscillators).to.deep.equal({})
      })


      it('does not allow adding oscillators without an existing receiver', function() {
        let eid = 'foo'
        let data = {
          receiver: eid,
          procedure: function() {}
        }

        function test () {
          manager.addOscillator(data)
        }

        expect(test).to["throw"](/receiver.*foo/)
      })


      it('adds the oscillator id to the receiver entity', function() {
        let eid = 'foo'
        let name = 'lululu'

        let oldEntity = manager.addEntity({
          id: eid,
          name: name
        })

        let oscillator = manager.addOscillator({
          receiver: eid,
          procedure: function() {}
        })

        let entity = manager.getSpec().entities[eid]

        expect(entity.oscillator).to.equal(oscillator.id)
        expect(oldEntity.oscillator).to.not.exist
      })
    })
  })


  describe('Queries', function() {

    describe('getEntityById', function() {

      it('retrieves an entity given its id', function() {

        manager.addEntity({
          id: 'foo',
          name: 'blub'
        })

        expect(manager.getEntityById('foo')).to.have.property('name', 'blub')
        expect(manager.getEntityById('bar')).to.not.exist
      })
    })


    describe('getEntityByNameNamespace', function() {

      it('retrieves entities by their name and namespace', function() {
        let e1 = manager.addEntity({
          name: 'foo'
        })

        let e2 = manager.addEntity({
          name: 'foo',
          namespace: 'biz'
        })

        let e3 = manager.addEntity({
          name: 'bar',
          namespace: 'biz'
        })

        expect(manager.getEntityByNameNamespace('foo')).to.deep.equal(e1)
        expect(manager.getEntityByNameNamespace('foo', 'biz')).to.deep.equal(e2)
        expect(manager.getEntityByNameNamespace('bar', 'biz')).to.deep.equal(e3)
        expect(manager.getEntityByNameNamespace('bar')).to.not.exist
      })
    })
  })


  describe('entity spec loader', function() {

    var sys

    beforeEach(function() {
      sys = Runtime.create()
    })


    it('can set entities', function() {
      let spec = {
        entities: [{
          id: 'myNewEntity'
        }]
      }

      Manager.create(spec).sendToRuntime(sys)

      let entity = sys.getEntity('myNewEntity')

      expect(entity).to.exist
    })


    it('can set many entities', function() {
      const spec = {
        entities: [{
          id: 'e1'
        }, {
          id: 'e2'
        }, {
          id: 'e3'
        }]
      }

      Manager.create(spec).sendToRuntime(sys)

      expect(sys.getEntity('e1')).to.exist
      expect(sys.getEntity('e2')).to.exist
      expect(sys.getEntity('e3')).to.exist
    })


    it('can set initial Values', function() {
      const spec = {
        entities: [{
          id: 'foo',
          initialValue: 'lala'
        }]
      }

      Manager.create(spec).sendToRuntime(sys)

      expect(sys.get('foo')).to.equal('lala')
    })


    it('can set a factory', function() {
      const spec = {
        entities: [{
          id: 'foo'
        }],
        factories: [{
          receiver: 'foo',
          procedure: function() {
            return 33
          }
        }]
      }

      Manager.create(spec).sendToRuntime(sys)

      expect(sys.get('foo')).to.equal(33)
    })


    it('can set a factory with dependencies', function() {
      const spec = {
        entities: [{
          id: 'bar',
          initialValue: 22
        }, {
          id: 'foo'
        }],
        factories: [{
          receiver: 'foo',
          procedure: function(bar) {
            return bar + 11
          },
          dependencies: ['bar']
        }]
      }

      Manager.create(spec).sendToRuntime(sys)

      expect(sys.get('foo')).to.equal(33)
    })


    it('can load entities with reactions', function() {
      const spec = {
        entities: [{
          id: 'bar',
          initialValue: 10
        }, {
          id: 'baz',
          initialValue: 20
        }],
        factories: [{
          receiver: 'foo',
          dependencies: ['baz'],
          procedure: function(baz) {
            return baz + 10
          }
        }],
        reactions: [{
          receiver: 'foo',
          triggers: ['bar'],
          procedure: function(foo, bar) {
            return foo + bar
          }
        }]
      }

      Manager.create(spec).sendToRuntime(sys)

      expect(sys.get('foo')).to.equal(40)

      sys.set('baz', 30)
      sys.flush()

      expect(sys.get('foo')).to.equal(50)
    })


    it('can handle objects instead of arrays, with random keys', function() {
      const spec = {
        entities: {
          'xuxu': {
            id: 'bar',
            initialValue: 10
          },
          'random2': {
            id: 'baz',
            initialValue: 20
          },
          'random3': {
            id: 'foo'
          },
          'random4': {
            id: 'kuku'
          },
          'random5': {
            id: 'ku',
            initialValue: 50
          }
        },
        factories: {
          'fufufu': {
            receiver: 'foo',
            dependencies: ['baz'],
            procedure: function(baz) {
              return baz + 10
            }
          }
        },
        reactions: {
          'kukuku': {
            receiver: 'foo',
            triggers: ['bar'],
            procedure: function(foo, bar) {
              return foo + bar
            }
          }
        },
        oscillators: {
          'kuku': {
            receiver: 'kuku',
            target: 'ku',
            procedure: function(sink) {
              sink(100)
              return 222
            }
          }
        }
      }

      Manager.create(spec).sendToRuntime(sys)

      expect(sys.get('foo')).to.equal(40)
      expect(sys.get('kuku')).to.equal(222)
      expect(sys.get('ku')).to.equal(100)
      expect(sys.get('bar')).to.equal(10)

      sys.set('baz', 30)
      sys.flush()

      expect(sys.get('foo')).to.equal(50)
    })
  })
})
