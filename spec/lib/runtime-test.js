var runtime = require('runtime')


describe('System runtime', function() {

  var sys

  beforeEach(function() {
    sys = runtime.create()
  })


  it('can expose its internal state', function() {
    expect(sys.getState())
      .to.contain.all.keys(['entities', 'actions', 'changes', 'calls'])
  })


  it('can set and get entity values', function() {
    let entities = sys.getState().entities

    expect(Object.keys(entities).length).to.equal(0)
    expect(sys.get('newVal')).to.be.undefined

    sys.set('newVal', 42)

    expect(Object.keys(entities).length).to.equal(1)
    expect(sys.get('newVal')).to.equal(42)
  })


  it('can retrieve entities', function() {
    sys.set('fufu', 'lululu')

    let entity = sys.getEntity('fufu')

    expect(entity).to.contain.all.keys(['id', 'value'])
    expect(entity).to.contain.all.keys({
      id: 'fufu',
      value: 'lululu'
    })
  })


  it('can explicitly add entities with an initial Value', function() {
    sys.addEntity({
      id: 'foo',
      initialValue: 22
    })

    expect(sys.get('foo')).to.equal(22)
  })


  it('updates by function', function() {
    sys.set('foo', 10)

    sys.update('foo', function(x) {
      return x + 3
    })

    expect(sys.get('foo')).to.equal(13)
  })


  it('can get and set an context', function() {
    expect(sys.getContext()).to.be["null"]

    sys.setContext(42)

    expect(sys.getContext()).to.equal(42)
  })


  describe('factory', function() {

    it('initializes entities programatically', function() {
      function factory () { return "newEntity" }

      sys.addFactory({
        procedure: factory,
        receiver: 'foo'
      })

      expect(sys.get('foo')).to.equal('newEntity')
    })


    it('can specify dependencies', function() {
      function factory (foo, bar) {
        return foo + bar
      }

      sys.set('foo', 3)
      sys.set('bar', 5)

      sys.addFactory({
        procedure: factory,
        receiver: 'test',
        dependencies: ['foo', 'bar']
      })

      expect(sys.get('test')).to.equal(8)
    })


    it('reruns on dependency change', function() {
      function init (x) {
        return x + 1
      }
      sys.set('x', 3)

      sys.addFactory({
        procedure: init,
        receiver: 'foo',
        dependencies: ['x']
      })

      expect(sys.get('foo')).to.equal(4)

      sys.set('x', 10)
      sys.flush()

      expect(sys.get('foo')).to.equal(11)
    })
  })


  describe('reactions', function() {

    it('can be defined explicitly', function() {
      function reaction (foo, x) {
        return foo + x
      }

      sys.set('x', 3)
      sys.set('foo', 1)

      sys.addReaction({
        procedure: reaction,
        receiver: 'foo',
        triggers: ['x']
      })

      expect(sys.get('foo')).to.equal(1)

      sys.flush()

      expect(sys.get('foo')).to.equal(4)

      sys.set('x', 5)
      sys.flush()

      expect(sys.get('foo')).to.equal(9)
    })


    it('can react on multiple dependencies', function() {
      sys.set('x', 3)
      sys.set('y', 4)
      sys.set('test', 2)

      function reaction (test, x, y) {
        return test + x - y
      }

      sys.addReaction({
        procedure: reaction,
        receiver: 'test',
        triggers: ['x', 'y']
      })

      sys.flush()

      expect(sys.get('test')).to.equal(1)
    })


    it('can have extra supplements that are not reactive', function() {
      sys.set('x', 1)
      sys.set('y', 2)

      sys.addReaction({
        procedure: function(z, x, y) {
          return x + y
        },
        receiver: 'z',
        triggers: ['x'],
        supplements: ['y']
      })

      sys.flush()

      expect(sys.get('z')).to.equal(3)

      sys.set('x', 2)
      sys.flush()

      expect(sys.get('z')).to.equal(4)

      sys.set('y', 5)
      sys.flush()

      expect(sys.get('z')).to.equal(4)

      sys.set('x', 3)
      sys.flush()

      expect(sys.get('z')).to.equal(8)
    })


    it('preserve reaction state after reinit reactions', function() {
      sys.set('foo', 'foo_value')
      sys.set('bar', 'bar_value')

      function init (bar) {
        return {
          myTest: 'test_value',
          myBar: bar
        }
      }

      function reaction (test, foo) {
        test.myFoo = foo
      }

      sys.addFactory({
        procedure: init,
        receiver: 'test',
        dependencies: ['bar']
      })

      sys.addReaction({
        procedure: reaction,
        receiver: 'test',
        triggers: ['foo']
      })

      sys.flush()

      expect(sys.get('test')).to.deep.equal({
        myTest: 'test_value',
        myFoo: 'foo_value',
        myBar: 'bar_value'
      })

      sys.set('bar', 'bar_new_value')
      sys.flush()

      expect(sys.get('test')).to.deep.equal({
        myTest: 'test_value',
        myFoo: 'foo_value',
        myBar: 'bar_new_value'
      })
    })


    it('calls reactions only once', function() {
      let reaction = sinon.stub()
      sys.set('foo', 'foo_value')
      sys.set('bar', 'bar_value')
      sys.set('baz', 'baz_value')

      sys.addFactory({
        procedure: function(bar) {
          return 'test_value'
        },
        receiver: 'test',
        dependencies: ['bar']
      })

      sys.addReaction({
        procedure: reaction,
        receiver: 'test',
        triggers: ['foo', 'baz']
      })

      sys.flush()

      expect(reaction).to.be.calledOnce

      reaction.reset()

      sys.set('foo', 'foo_new_value')
      sys.set('bar', 'bar_new_value')
      sys.set('baz', 'baz_new_value')
      sys.flush()

      expect(reaction).to.be.calledOnce
      expect(reaction).to.be.calledWith('test_value', 'foo_new_value', 'baz_new_value')
    })


    it('can be triggered by a touch', function() {
      sys.set('counter', 0)

      sys.addReaction({
        procedure: function(counter) {
          return counter + 1
        },
        receiver: 'counter',
        triggers: ['trigger']
      })

      sys.flush()

      expect(sys.get('counter')).to.equal(1)

      for (let i = 0; i < 10; i++) {
        sys.touch('trigger')
        sys.flush()
      }

      expect(sys.get('counter')).to.equal(11)
    })
  })


  describe('actions', function() {

    it('can be added to system and called', function() {
      sys.set('foo', 3)
      sys.set('bar', 4)
      function action (foo, bar) {
        return "foobar" + (foo + bar)
      }

      let id = sys.addAction({
        procedure: action,
        dependencies: ['foo', 'bar']
      }).id

      expect(id).to.be.a('string')
      expect(sys.getState().actions[id]).to.exist
      expect(sys.callAction(id)).to.equal('foobar7')
    })
  })


  describe('callbacks', function() {

    it('can be added to system', function() {
      sys.set('foo', 3)
      sys.set('bar', 4)
      function callback (foo, bar) {
        return "foobar"
      }

      let id = sys.addCallback({
        procedure: callback,
        triggers: ['foo', 'bar']
      }).id

      expect(id).to.be.a('string')
      expect(sys.getState().actions[id]).to.exist
      expect(sys.callAction(id)).to.equal('foobar')
      expect(sys.getEntity('foo').callbacks.length).to.equal(1)
      expect(sys.getEntity('bar').callbacks.length).to.equal(1)
    })


    it('are called on entity change', function() {
      let cb = sinon.stub()
      sys.set('foo', 10)

      sys.addFactory({
        procedure: function(foo) {
          return foo + 2
        },
        receiver: 'bar',
        dependencies: ['foo']
      })

      sys.addCallback({
        procedure: cb,
        triggers: ['bar']
      })

      sys.update('foo', function(foo) {
        return foo - 5
      })

      sys.flush()

      expect(sys.get('bar')).to.equal(7)
      expect(cb).to.be.calledWith(7)
    })


    it('can be more than one', function() {
      let cb1 = sinon.stub(),
          cb2 = sinon.stub()

      sys.set('foo', 'foo_value')

      sys.addCallback({
        procedure: cb1,
        triggers: ['foo']
      })

      sys.addCallback({
        procedure: cb2,
        triggers: ['foo']
      })

      sys.set('foo', 'new_foo_value')
      sys.flush()

      expect(cb1).to.be.calledWith('new_foo_value')
      expect(cb2).to.be.calledWith('new_foo_value')
    })


    it('is called only once even if registered for many entities', function() {
      let cb = sinon.stub()
      sys.set('foo', 'foo_value')
      sys.set('bar', 'bar_value')

      sys.addCallback({
        procedure: cb,
        triggers: ['foo', 'bar']
      })

      sys.set('foo', 'new_foo_value')
      sys.set('bar', 'new_bar_value')
      sys.flush()

      expect(cb).to.be.calledOnce
      expect(cb).to.be.calledWith('new_foo_value', 'new_bar_value')
    })


    it('can be removed', function() {
      let cb = sinon.stub()

      let id = sys.addCallback({
        procedure: cb,
        triggers: ['foo']
      }).id

      sys.set('foo', 'foo_value')
      sys.flush()

      expect(cb).to.be.called

      cb.reset()

      sys.set('foo', 'new_foo_value')
      sys.removeCallback(id)
      sys.flush()

      expect(cb).to.not.be.called
      expect(sys.getState().actions[id]).to.not.be.defined
      expect(sys.getEntity('foo').callbacks.length).to.equal(0)
    })


    it('can have supplements', function() {
      let cb = sinon.stub()
      sys.set('foo', 40)
      sys.set('bar', 50)

      sys.addCallback({
        procedure: cb,
        triggers: ['foo'],
        supplements: ['bar']
      })

      sys.set('bar', 5)
      sys.flush()

      expect(cb).to.not.be.called

      sys.set('foo', 4)
      sys.flush()

      expect(cb).to.be.calledWith(4, 5)
    })
  })


  describe('procedure context', function() {

    it('is accessible in actions', function() {
      sys.setContext({
        myFunc: function() {
          return 40
        }
      })

      sys.set('foo', 2)

      let action = sys.addAction({
        procedure: function(foo) {
          return foo + this.myFunc()
        },
        dependencies: ['foo']
      })

      expect(sys.callAction(action.id)).to.equal(42)
    })


    it('is accessible in factories', function() {
      sys.setContext({
        myFunc: function() {
          return 43
        }
      })

      sys.addFactory({
        procedure: function() {
          return this.myFunc()
        },
        receiver: 'foo'
      })

      expect(sys.get('foo')).to.equal(43)
    })


    it('is accessible in reactions', function() {
      sys.setContext({
        myFunc: function() {
          return 40
        }
      })

      let reaction = sys.addReaction({
        procedure: function(bar, foo) {
          return foo + this.myFunc()
        },
        triggers: ['foo'],
        receiver: 'bar'
      })

      sys.set('foo', 2)
      sys.flush()

      expect(sys.callAction(reaction.id)).to.equal(42)
    })
  })


  describe('Oscillators', function() {

    it('can be added to the system', function() {
      let oscillator = sys.addOscillator({
        receiver: 'foo',
        target: 'bar',
        procedure: sinon.stub()
      })

      let state = sys.getState()

      expect(state.actions[oscillator.id]).to.equal(oscillator)
      expect(state.entities['foo'].id).to.equal('foo')
      expect(state.entities['foo'].oscillator).to.equal(oscillator.id)
    })


    it('\'s procedure is called with a sink that sets the target value', function() {
      let sink
      function procedure (s) {
        sink = s
      }

      sys.addOscillator({
        receiver: 'foo',
        target: 'bar',
        procedure: procedure
      })

      expect(sink).to.be.a('function')
      expect(sys.get('bar')).to.not.exist

      sink(42)

      expect(sys.get('bar')).to.equal(42)

      sink("huhu")

      expect(sys.get('bar')).to.equal('huhu')
    })


    it('can flush manually when sinking', function() {
      let sink
      function procedure (s) {
        sink = s
      }

      sys.addOscillator({
        receiver: 'foo',
        target: 'bar',
        procedure: procedure
      })

      sys.addReaction({
        procedure: function(_, bar) {
          return bar
        },
        receiver: 'bazz',
        triggers: ['bar']
      })

      expect(sys.get('bazz')).to.not.exist

      sink(42)

      expect(sys.get('bazz')).to.not.exist

      sys.flush()

      expect(sys.get('bazz')).to.equal(42)

      sink("huhu", true)

      expect(sys.get('bazz')).to.equal('huhu')
    })


    it('can create a receiver value', function() {
      function procedure () {
        return 42
      }

      sys.addOscillator({
        receiver: 'foo',
        procedure: procedure
      })

      expect(sys.get('foo')).to.equal(42)
    })


    it('can have dependencies', function() {
      sys.set('foo', 7)
      sys.set('bar', 3)

      sys.addOscillator({
        receiver: 'receiver',
        target: 'target',
        dependencies: ['foo', 'bar'],
        procedure: function(sink, foo, bar) {
          sink(foo + bar)
          return foo + bar + 1
        }
      })

      expect(sys.get('receiver')).to.equal(11)
      expect(sys.get('target')).to.equal(10)
    })


    it('recreates itself on dependency change', function() {
      sys.set('foo', 7)

      sys.addOscillator({
        receiver: 'receiver',
        target: 'target',
        dependencies: ['foo'],
        procedure: function(sink, foo) {
          return foo + 1
        }
      })

      expect(sys.get('receiver')).to.equal(8)

      sys.set('foo', 11)
      sys.flush()

      expect(sys.get('receiver')).to.equal(12)
    })


    it('called the return function before beeing recreated', function() {

      let returnFunction1 = sinon.stub(),
          returnFunction2 = sinon.stub(),
          proc = sinon.stub()

      proc.onCall(0).returns(returnFunction1)
      proc.onCall(1).returns(returnFunction2)
      proc.returns(42)

      sys.set('foo', 'kuku')

      sys.addOscillator({
        receiver: 'receiver',
        target: 'target',
        dependencies: ['foo'],
        procedure: proc
      })

      expect(sys.get('receiver')).to.equal(returnFunction1)
      expect(returnFunction1).not.to.be.called

      sys.touch('foo')

      expect(returnFunction1).not.to.be.called

      sys.flush()

      expect(returnFunction1).to.be.called
      expect(sys.get('receiver')).to.equal(returnFunction2)

      sys.touch('foo')

      expect(returnFunction2).not.to.be.called

      sys.flush()

      expect(returnFunction2).to.be.called
      expect(sys.get('receiver')).to.equal(42)
      expect(returnFunction1).to.be.calledOnce
      expect(returnFunction2).to.be.calledOnce
    })
  })
})
