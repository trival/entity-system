var types = require('spec-types')


describe("Spec types", function() {

  it("has default value type", function() {
    expect(types.DEFAULT_VALUE_TYPE).to.be.a('string')
  })


  describe("Entity creation", function() {

    const createEntity = types.createEntity

    it("requires name to be set", function() {
      function test1 () { createEntity({}) }
      function test2 () { createEntity({ id: 'fuu' }) }

      expect(test1).to["throw"](/name/)
      expect(test2).to["throw"](/name/)
    })


    it('adds default uuid, namespace and valueType', function() {
      let result = createEntity({
        name: "foo"
      })

      expect(result).to.containSubset({
        name: 'foo',
        namespace: "",
        valueType: types.DEFAULT_VALUE_TYPE
      })

      expect(result.id).to.be.a('string').that.not.contains('foo')
    })


    it('creates entities with a unique id', function() {
      let e1 = createEntity({
        name: 'foo'
      })
      let e2 = createEntity({
        name: 'foo'
      })

      expect(e1.id).to.not.equal(e2.id)
    })


    it("can change its valueType", function() {
      let e = createEntity({
        name: "foo",
        valueType: "TEST"
      })

      expect(e.valueType).to.equal("TEST")
    })


    it("has optional properties initialValue, factory, oscillator, reactions", function() {
      let e = createEntity({
        name: "fooo",
        initialValue: 666
      })

      expect(e.initialValue).to.equal(666)

      e = createEntity({
        name: "fooo",
        reactions: 777
      })

      expect(e.reactions).to.equal(777)

      e = createEntity({
        name: "fooo",
        factory: 888
      })

      expect(e.factory).to.equal(888)

      e = createEntity({
        name: "fooo",
        oscillator: 999
      })

      expect(e.oscillator).to.equal(999)
    })


    it("ignores other properties", function() {
      let e = createEntity({
        name: "fooo",
        foo: "lala",
        bar: "kuku"
      })

      expect(e).to.not.contain.any.keys('bar', 'foo')
    })
  })


  describe("Action creation", function() {

    const createAction = types.createAction

    it("requires a procedure or code property", function() {
      function test () { createAction({}) }

      expect(test).to["throw"](/procedure.*code/)
    })


    it("calculates code from procedure", function() {
      function procedure () {}

      expect(createAction({ procedure }))
      .to.deep.equal({
        procedure,
        code: procedure.toString()
      })
    })


    it("calucates procedure from code", function() {
      let code = "function (x) {return x + 1}"
      let result = createAction({ code })

      expect(result.code).to.equal(code)
      expect(result.procedure).to.be.a('function')
      expect(result.procedure(2)).to.equal(3)
    })


    it("doesn't change code or procedure if present", function() {
      let code = 'fufu'
      function procedure () {}

      let result = createAction({ code, procedure })

      expect(result.code).to.equal(code)
      expect(result.procedure).to.equal(procedure)
    })


    it("checks procedure to be a function", function() {
      function test1 () { createAction({ code: '"fufu"' }) }
      function test2 () { createAction({ procedure: 22 }) }

      expect(test1).to["throw"](/procedure.*function/)
      expect(test2).to["throw"](/procedure.*function/)
    })


    it("selects the evaluation method by a given dispatch function", function() {
      let proc1 = function() {},
          proc2 = function() {},
          proc3 = function() {},
          code1 = "fuu",
          code2 = "baa",
          code3 = "kuku",
          language1 = "JS",
          language2 = "Coffee",
          method1 = sinon.stub().withArgs(code1).returns(proc1),
          method2 = sinon.stub().withArgs(code2).returns(proc2),
          method3 = sinon.stub().withArgs(code3).returns(proc3)

      let dispatcher = function(language) {
        switch (language) {
          case language1:
            return method1
          case language2:
            return method2
          default:
            return method3
        }
      }

      let result1 = createAction({
        code: code1,
        language: language1
      }, dispatcher)

      let result2 = createAction({
        code: code2,
        language: language2
      }, dispatcher)

      let result3 = createAction({
        code: code3
      }, dispatcher)

      expect(result1.language).to.equal(language1)
      expect(result2.language).to.equal(language2)
      expect(result1.procedure).to.equal(proc1)
      expect(result2.procedure).to.equal(proc2)
      expect(result3.procedure).to.equal(proc3)
      expect(method1).to.be.called
      expect(method2).to.be.called
      expect(method3).to.be.called
    })


    it('evaluates standard javascript when no evaluator selector is set', function() {
      let result = createAction({
        code: "function () {return 'fufu'}"
      })

      expect(result.procedure()).to.equal('fufu')
    })
  })


  describe("Factory creation", function() {

    var createFactory = types.createFactory

    it("requires a receiver", function() {
      function test () { createFactory({}) }

      expect(test).to["throw"](/receiver.*factory/)
    })


    it("generates a minimal attribute set with id, procedure, receiver and code", function() {
      function procedure () {}
      let receiver = 'foo'

      let factory = createFactory({ procedure, receiver })

      expect(factory).to.containSubset({
        procedure,
        receiver,
        code: procedure.toString()
      })

      expect(factory.id).to.be.a('string')
    })


    it("can have custom id", function() {
      expect(createFactory({
        procedure: function() {},
        receiver: 'foo',
        id: 'lulu'
      })).to.have.property('id', 'lulu')
    })


    it("has optional dependency attribute", function() {
      let dependencies = ['foo', 'bar']

      expect(createFactory({
        procedure: function(foo, bar) {},
        receiver: 'biz',
        dependencies: dependencies
      })).to.have.property('dependencies', dependencies)
    })


    it("selects the evaluation method by a given dispatch function", function() {
      function proc () {}
      let code = "fuu"
      let language = "JS"
      let method = sinon.stub().withArgs(code).returns(proc)

      let dispatcher = function(lang) {
        switch (lang) {
          case language:
            return method
          default:
            throw Error('this should not happen')
        }
      }

      let result = createFactory({
        code,
        language,
        receiver: 'foo'
      }, dispatcher)

      expect(result.language).to.equal(language)
      expect(result.procedure).to.equal(proc)
      expect(method).to.be.called
    })
  })


  describe("Reaction creation", function() {

    const createReaction = types.createReaction

    it("requires a receiver", function() {
      function test () {
        createReaction({
          procedure: function() {}
        })
      }

      expect(test).to["throw"](/receiver.*reaction/)
    })


    it("requires at least one trigger", function() {
      function test1 () {
        createReaction({
          procedure: function() {},
          receiver: 'foo'
        })
      }

      function test2 () {
        createReaction({
          procedure: function() {},
          receiver: 'foo',
          triggers: []
        })
      }

      expect(test1).to["throw"](/triggers.*reaction/)
      expect(test2).to["throw"](/triggers.*reaction/)
    })


    it("generates a minimal attribute set with id, procedure, receiver", function() {
      function procedure (foo, bar) {}
      let receiver = 'foo'
      let triggers = ['bar']

      let reaction = createReaction({
        procedure,
        receiver,
        triggers
      })

      expect(reaction).to.containSubset({
        procedure,
        receiver,
        code: procedure.toString()
      })

      expect(reaction.id).to.be.a('string')
    })


    it("can have custom id", function() {
      expect(createReaction({
        procedure: function(foo, bar) {},
        receiver: 'foo',
        triggers: ['bar'],
        id: 'lulu'
      })).to.have.property('id', 'lulu')
    })


    it("has optional supplements attribute", function() {
      let supplements = ['foo', 'bar']
      expect(createReaction({
        procedure: function(biz, kuku, foo, bar) {},
        receiver: 'biz',
        triggers: ['kuku'],
        supplements: supplements
      })).to.have.property('supplements', supplements)
    })


    it("selects the evaluation method by a given dispatch function", function() {
      function proc (foo, bar) {}
      let code = "fuu"
      let language = "JS"
      let method = sinon.stub().withArgs(code).returns(proc)

      let dispatcher = function(language) {
        switch (language) {
          case language:
            return method
          default:
            throw Error("this should not happen")
        }
      }

      let result = createReaction({
        code,
        language,
        receiver: 'foo',
        triggers: ['bar']
      }, dispatcher)

      expect(result.language).to.equal(language)
      expect(result.procedure).to.equal(proc)
      expect(method).to.be.called
    })


    it('is chainable without changing the value', function() {
      let r1 = createReaction({
        receiver: 'foo',
        procedure: function(foo, bar) {},
        triggers: ['bar']
      })
      let r2 = createReaction(r1)
      let r3 = createReaction(r2)

      expect(r1).to.not.be.equal(r2)
      expect(r1).to.be.deep.equal(r2)
      expect(r1).to.be.deep.equal(r3)
    })
  })


  describe("Oscillator creation", function() {

    const createOscillator = types.createOscillator

    it("requires a receiver", function() {
      function test () { createOscillator({}) }

      expect(test).to["throw"](/receiver.*oscillator/)
    })


    it("generates a minimal attribute set with id, procedure, receiver and code", function() {
      function procedure () {}
      let receiver = 'foo'

      let oscillator = createOscillator({
        procedure,
        receiver
      })

      expect(oscillator).to.containSubset({
        procedure,
        receiver,
        code: procedure.toString()
      })

      expect(oscillator.id).to.be.a('string')
    })


    it("can have custom id", function() {
      expect(createOscillator({
        procedure: function() {},
        receiver: 'foo',
        id: 'lulu'
      })).to.have.property('id', 'lulu')
    })


    it("has optional dependency attribute", function() {
      let dependencies = ['foo', 'bar']

      expect(createOscillator({
        procedure: function(foo, bar) {},
        receiver: 'biz',
        dependencies: dependencies
      })).to.have.property('dependencies', dependencies)
    })


    it("has optional target attribute", function() {
      let target = 'foo'

      expect(createOscillator({
        procedure: function() {},
        receiver: 'biz',
        target: target
      })).to.have.property('target', target)
    })


    it("selects the evaluation method by a given dispatch function", function() {
      function proc () {}
      let code = "fuu"
      let language = "JS"
      let method = sinon.stub().withArgs(code).returns(proc)

      let dispatcher = function(lang) {
        switch (lang) {
          case language:
            return method
          default:
            throw Error('this should not happen')
        }
      }

      let result = createOscillator({
        code,
        language,
        receiver: 'foo'
      }, dispatcher)

      expect(result.language).to.equal(language)
      expect(result.procedure).to.equal(proc)
      expect(method).to.be.called
    })
  })
})
