var Helpers = require('utils/functional-helpers'),
    originalAssign = require('object-assign'),
    originalUpdate = require('react-addons-update')


describe("Functional helpers", function() {

  describe("assign", function() {

    it("is the same as object-assign", function() {
      expect(Helpers.assign).to.equal(originalAssign)
    })
  })


  describe("update", function() {

    it("is the same as react-addons-update", function() {
      expect(Helpers.update).to.equal(originalUpdate)
    })
  })


  describe("extend", function() {

    it("merges objects into a new object", function() {
      let obj1 = {
        id: 'foo',
        name: 'lala'
      }
      let obj2 = {
        name: 'kuku',
        namespace: 'bar'
      }
      let obj3 = {
        id: 'faaa',
        name: 'buzzz',
        factory: 'fumfum'
      }

      let res1 = Helpers.extend(obj1, obj2)

      expect(res1).to.deep.equal(Helpers.assign({}, obj1, obj2))

      expect(res1).to.not.equal(obj1)

      // obj 1 unchanged
      expect(obj1).to.deep.equal({
        id: 'foo',
        name: 'lala'
      })

      expect(res1).to.deep.equal({
        id: 'foo',
        name: 'kuku',
        namespace: 'bar'
      })

      expect(Helpers.extend(obj1, obj2, obj3))
        .to.deep.equal(Helpers.assign({}, obj1, obj2, obj3))
    })
  })
})
