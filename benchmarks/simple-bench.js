'use strict';

var flow = require('../build/tvs-es')


const UPDATES = 100000

function inc (x) {
  return x + 1
}

function length (x) {
  return x.length
}


const definition = {

  'counter': {
    value: 0,
    reactions: {
      'trigger': inc
    }},

  'counterAcc': {
    value: [],
    reactions: {
      'counter': function(acc, cnt) {
        acc.push(cnt)
      }
    }},

  'accLength': {
    init: {
      require: 'counterAcc',
      "do": length
    }}

}

let sys = flow.Runtime.create(),
    spec = flow.SpecManager.create()

flow.loaders.ObjectSpecParser.load(definition, spec)
spec.sendToRuntime(sys)

let startTime = Date.now(),
    id = spec.getEntityByNameNamespace('trigger').id

for (let i = 0; i < UPDATES; i++) {
  sys.touch(id)
  sys.flush()
}

let duration1 = Date.now() - startTime

startTime = Date.now()

let counter = 0,
    counterAcc = [],
    accLength

for (let i = 0; i < UPDATES; i++) {
  counter = inc(counter)
  counterAcc.push(counter)
  accLength = length(counterAcc)
}

let duration2 = Date.now() - startTime


//console.log("system state: ", sys.getState())
console.log(UPDATES + " ES updates in " + duration1 + " ms")
console.log("imperative state", {counter, accLength})
console.log("compared to vanilla js " + duration2 + " ms")
