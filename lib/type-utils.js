var newUid = (function() {
  var id = 1
  return function() {
    return id++
  }
})()


function getEntityKeyFromNameNamespace (name, namespace) {
  if (!namespace) return name
  return [namespace, name].join('/')
}


function getNameNamespaceFromEntityKey (id) {
  let [namespace, name] = id.split('/')
  if (!name) {
    name = namespace
    namespace = ''
  }
  return [name, namespace]
}


// ===== module interface =====

module.exports = {
  newUid,
  getNameNamespaceFromEntityKey,
  getEntityKeyFromNameNamespace
}
