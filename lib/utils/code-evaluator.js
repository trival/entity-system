function evaluate(code, context) {
  const prefix = "(function(){ return "
  const postfix = "})"
  const factory = eval(prefix + code + postfix)
  return factory.call(context)
}


module.exports = {
  evaluate
}
