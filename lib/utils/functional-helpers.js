var assign = require('object-assign'),
    update = require('react-addons-update')


function extend() {
  var args = [{}]
  for (var i = 0, len = arguments.length; i < len; i++) {
    args.push(arguments[i])
  }
  return assign.apply(null, args)
}


module.exports = {
  extend,
  assign,
  update
}

