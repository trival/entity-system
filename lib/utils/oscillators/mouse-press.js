module.exports = function createMousePress(opts) {

  let {
    element = document,
    autoFlush,
    enableRightButton
  } = opts || {}


  return function(sink) {

    var state = {
      leftButton: false,
      middleButton: false,
      rightButton: false
    }

    sink(state)

    function onMouseUp(e) {
      e.preventDefault()

      state.leftButton = false
      state.middleButton = false
      state.rightButton = false

      sink(state, autoFlush)
    }


    function onMouseDown(e) {
      e.preventDefault()

      switch (e.button) {
        case 0:
          state.leftButton = true; break
        case 1:
          state.middleButton = true; break
        case 2:
          state.rightButton = true
      }

      sink(state, autoFlush)
    }


    element.addEventListener("mousedown", onMouseDown)
    document.addEventListener("mouseup", onMouseUp)

    if (enableRightButton) {
      element.addEventListener("contextmenu", e => e.preventDefault())
    }
  }

}

