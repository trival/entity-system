module.exports = function createAnimationFrame(opts) {

  let {autoFlush} = opts || {}


  return function (sink) {

    var isPlaying = true,
        oldTime = Date.now(),
        newTime = null

    function next() {
      if (isPlaying) {
        newTime = Date.now()
        sink(newTime - oldTime, autoFlush)
        oldTime = newTime
        requestAnimationFrame(next)
      }
    }

    next()

    return function stop() {
      isPlaying = false
    }
  }

}
