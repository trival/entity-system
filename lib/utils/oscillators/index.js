var directionKeys    = require('./direction-keys'),
    mouseDrag        = require('./mouse-drag'),
    mousePress       = require('./mouse-press'),
    requestAnimation = require('./animation-frame')


module.exports = {
  directionKeys,
  mouseDrag,
  mousePress,
  requestAnimation
}
