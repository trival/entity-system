module.exports = function createMouseDrag(opts) {

  let {
    autoFlush,
    element = document
  } = opts || {}


  return function(sink) {

    var x = 0,
        y = 0,
        dragging = false,
        oldDeltaX = 0,
        oldDeltaY = 0

    var delta = { x: 0, y: 0 }

    sink(delta)


    function onMouseMove(e) {
      if (dragging) {
        oldDeltaX = delta.x
        oldDeltaY = delta.y
        delta.x = x - e.screenX
        delta.y = y - e.screenY
        if (delta.x !== oldDeltaX || delta.y !== oldDeltaY) {
          sink(delta, autoFlush)
        }
        x = e.screenX
        y = e.screenY
      }
    }


    function onMouseUp() {
      delta.x = oldDeltaX = 0
      delta.y = oldDeltaY = 0
      sink(delta, autoFlush)
      dragging = false
    }


    function onMouseDown (e) {
      if (e.button === 0) {
        x = e.screenX
        y = e.screenY
        dragging = true
      }
    }


    element.addEventListener("mousedown", onMouseDown)
    document.addEventListener("mousemove", onMouseMove)
    document.addEventListener("mouseup", onMouseUp)
  }
}

