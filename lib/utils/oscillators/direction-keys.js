module.exports = function createDirectionkeys(opts) {

  let {autoFlush} = opts || {}


  return function(sink) {

    var state = {
      forth: false,
      back: false,
      left: false,
      right: false,
      up: false,
      down: false
    }

    sink(state)


    function onKeyDown(e) {
      var update = false

      switch (e.keyCode) {
        case 38:
        case 87:
          if (!state.forth) {
            state.forth = true
            update = true
          }
          break

        case 37:
        case 65:
          if (!state.left) {
            state.left = true
            update = true
          }
          break

        case 40:
        case 83:
          if (!state.back) {
            state.back = true
            update = true
          }
          break

        case 39:
        case 68:
          if (!state.right) {
            state.right = true
            update = true
          }
          break

        case 82:
          if (!state.up) {
            state.up = true
            update = true
          }
          break

        case 70:
          if (!state.down) {
            state.down = true
            update = true
          }
      }

      if (update) {
        sink(state, autoFlush)
      }
    }


    function onKeyUp(e) {
      var update = false

      switch (e.keyCode) {
        case 38:
        case 87:
          state.forth = false
          update = true
          break

        case 37:
        case 65:
          state.left = false
          update = true
          break

        case 40:
        case 83:
          state.back = false
          update = true
          break

        case 39:
        case 68:
          state.right = false
          update = true
          break

        case 82:
          state.up = false
          update = true
          break

        case 70:
          state.down = false
          update = true
      }

      if (update) {
        sink(state, autoFlush)
      }
    }


    document.addEventListener("keyup", onKeyUp, false)
    document.addEventListener("keydown", onKeyDown, false)
  }
}
