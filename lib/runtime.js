var types = require('./runtime-types')


// ===== runtime factory =====

// Generates an empty entity system runtime.
module.exports.create = function() {

  // ===== permanent state =====

  // All entity data.
  var entities = {}

  // All actions operating on the entities.
  var actions = {}

  // The context that is accessible in action proceures
  var context = null


  // ===== temporal state =====

  // Stores all changes and processes them at flush.
  // Empty after each flush.
  var changes = {}

  // Callbacks executed after changes are processed.
  // Empty after each flush.
  var calls = {}


  function getState () {
    return { entities, actions, changes, calls }
  }


  function getContext () {
    return context
  }


  function setContext (ctx) {
    context = ctx
  }


  // ===== system topology updates =====

  function addEntity () {
    const entity = types.createEntity(...arguments)
    return entities[entity.id] = entity
  }


  function removeEntity (eid) {
    throw new Error('remove Entity not yet implemented')
  }


  function addFactory () {
    const factory = types.createFactory(...arguments)
    setupAction(factory)
    const entity = getOrCreateEntity(factory.receiver)
    if (factory.dependencies.length) {
      entity.factory = factory.id
      updateEntityReactions(entity, factory.id, factory.dependencies)
    }
    entity.value = executeAction(factory)
    propagateChange(entity)
    return factory
  }


  function removeFactory (eid) {
    throw new Error('remove Factory not yet implemented')
  }


  function addReaction () {
    const reaction = types.createReaction.apply(types, arguments)
    setupAction(reaction)
    const entity = getOrCreateEntity(reaction.receiver)
    updateEntityReactions(entity, reaction.id, reaction.triggers)
    for (let depid in entity.dependencies) {
      touch(depid)
    }
    return reaction
  }


  function removeReaction (eid) {
    throw new Error('remove Reaction not yet implemented')
  }


  function updateEntityReactions (entity, actionId, dependencies) {
    entity.dependencies = entity.dependencies || {}

    for (let i in dependencies) {
      let depid = dependencies[i]
      entity.dependencies[depid] = actionId
      let depEntity = getOrCreateEntity(depid)
      depEntity.effects = depEntity.effects || {}
      depEntity.effects[entity.id] = actionId
    }
  }


  function addAction () {
    return setupAction(types.createAction(...arguments))
  }


  function removeAction (eid) {
    throw new Error('remove Action not yet implemented')
  }


  function addCallback () {
    const callback = types.createCallback(...arguments)
    setupAction(callback)
    for (let i in callback.triggers) {
      let entity = getOrCreateEntity(callback.triggers[i])
      entity.callbacks = entity.callbacks || []
      if (entity.callbacks.indexOf(callback.id) < 0) {
        entity.callbacks.push(callback.id)
      }
    }
    return callback
  }


  function removeCallback (cid) {
    const deps = actions[cid].triggers
    for (let i in deps) {
      let e = entities[deps[i]]
      e.callbacks = e.callbacks.filter(cb => cb !== cid)
    }
    delete calls[cid]
    delete actions[cid]
  }


  function addOscillator () {
    const oscillator = types.createOscillator(...arguments)
    setupAction(oscillator)
    const receiver = getOrCreateEntity(oscillator.receiver)
    receiver.oscillator = oscillator.id
    oscillator.sink = function(val, doFlush) {
      set(oscillator.target, val)
      if (doFlush) flush()
    }

    if (oscillator.dependencies.length) {
      updateEntityReactions(receiver, oscillator.id, oscillator.dependencies)
    }

    receiver.value = executeAction(oscillator)
    return oscillator
  }


  function removeOscillator () {}


  function setupAction (action) {
    return actions[action.id] = action
  }


  // ===== interacting with entities =====

  function get (eid) {
    if (entities[eid] != null) return entities[eid].value
  }


  function set (eid, val) {
    const entity = getOrCreateEntity(eid)
    entity.value = val
    propagateChange(entity)
  }


  function update (eid, fn) {
    set(eid, fn(get(eid)))
  }


  function touch (eid) {
    propagateChange(entities[eid])
  }


  function getOrCreateEntity (eid) {
    return entities[eid] || addEntity({ id: eid })
  }


  function getEntity (eid) {
    return entities[eid]
  }


  // ===== change management =====

  function updateChange (entity, aid, order) {
    let change = changes[aid] != null ? changes[aid] : changes[aid] = {
      order,
      entity
    }
    if (change.order < order) {
      change.order = order
    }
  }


  function propagateChange (entity, order) {
    if (entity.effects != null) {
      order = order || 1

      for (let eid in entity.effects) {
        let aid = entity.effects[eid],
            nextEntity = entities[eid]
        updateChange(nextEntity, aid, order)

        // restore reactions state after reinitialization
        if (actions[aid].type === types.ActionTypes.FACTORY) {
          for (let i in nextEntity.dependencies) {
            updateChange(nextEntity, nextEntity.dependencies[i], order)
          }
        }

        propagateChange(nextEntity, order + 1)
      }
    }

    // handle callbacks
    if (entity.callbacks && entity.callbacks.length) {
      for (let i = 0; i < entity.callbacks.length; i++) {
        calls[entity.callbacks[i]] = true
      }
    }
  }



  function flush () {
    let process = {}
    for (let aid in changes) {
      let change = changes[aid],
          action = actions[aid],
          steps = process[change.order] || (process[change.order] = []),
          unit = {
            action,
            entity: change.entity
          }
      if (action.type === types.ActionTypes.FACTORY) {
        steps.unshift(unit)
      } else {
        steps.push(unit)
      }
    }

    changes = {}

    for (let order in process) {
      let steps = process[order]
      for (let i = 0; i < steps.length; i++) {
        let {action, entity} = steps[i],
            res = executeAction(action)
        if (res != null) {
          entity.value = res
        } else if (res === null) {
          console.warn('TODO: implement propagation stop!')
        }
      }
    }

    for (let cid in calls) {
      executeAction(actions[cid])
    }

    calls = {}
  }


  function executeAction (action) {
    let args = []

    for (let i = 0; i < action.dependencies.length; i++) {
      args[i] = entities[action.dependencies[i]].value
    }

    // handle oscillators
    if (action.sink) {
      let osc = get(action.receiver)
      if (osc && typeof osc === 'function') osc()
      args.unshift(action.sink)
    }

    return action.procedure.apply(context, args)
  }


  function callAction (actionId) {
    return executeAction(actions[actionId])
  }


  // ===== system interface =====

  return {
    get,
    set,
    touch,
    update,

    addEntity,
    removeEntity,
    addFactory,
    removeFactory,
    addReaction,
    removeReaction,
    addAction,
    removeAction,
    addCallback,
    removeCallback,
    addOscillator,
    removeOscillator,

    flush,
    callAction,

    getEntity,
    getState,

    getContext,
    setContext
  }
}
