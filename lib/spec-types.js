var uuid = require('uuid'),
    Evaluator = require('./utils/code-evaluator')


// ===== Defaults =====

const DEFAULT_VALUE_TYPE = "JSON"


// ===== Entity creation =====

function createEntity (spec) {

  // all entity properties
  let {
    id = uuid.v4(),
    name,
    namespace = '',
    valueType = DEFAULT_VALUE_TYPE,
    initialValue,
    factory,
    reactions,
    oscillator
  } = spec

  // required properties
  if (name == null) {
    throw Error("name must be set on this entity " + spec)
  }

  // minimal attribute set
  const entity = {
    id,
    name,
    namespace,
    valueType
  }

  // optional attributes
  if (initialValue != null) entity.initialValue = initialValue
  if (factory != null) entity.factory = factory
  if (oscillator != null) entity.oscillator = oscillator
  if (reactions != null) entity.reactions = reactions

  return entity
}


function createAction (spec, evaluatorSelector) {

  // all entity properties
  let {
    procedure,
    code,
    language
  } = spec

  // required properties
  if (!((procedure != null) || (code != null))) {
    throw Error("procedure or code property must be set on this action " + spec)
  }

  // calculated values
  if (code == null) code = procedure.toString()
  if (procedure == null) {
    let evaluate = (evaluatorSelector && evaluatorSelector(language)) || Evaluator.evaluate
    procedure = evaluate(code)
  }

  // predicates
  if (typeof procedure !== 'function') {
    throw Error("procedure must be a valid functions on this action " + spec)
  }

  // minimal attribute set
  const action = {
    procedure,
    code
  }

  // optional properties
  if (language != null) action.language = language

  return action
}


function createFactory (spec, evaluatorSelector) {

  // all entity properties
  let {
    id = uuid.v4(),
    receiver,
    dependencies
  } = spec

  // required properties
  if (receiver == null) {
    throw Error("receiver must be set on this factory " + spec)
  }

  const factory = createAction(spec, evaluatorSelector)

  // minimal attribute set
  factory.id = id
  factory.receiver = receiver

  // optional properties
  if (dependencies != null) factory.dependencies = dependencies

  return factory
}


function createReaction (spec, evaluatorSelector) {

  // all entity properties
  let {
    id = uuid.v4(),
    receiver,
    triggers,
    supplements
  } = spec

  // required properties
  if (receiver == null) {
    throw Error("receiver must be set on this reaction " + spec)
  }
  if (!(triggers && triggers.length)) {
    throw Error("triggers must have at least one element on this reaction " + spec)
  }

  const reaction = createAction(spec, evaluatorSelector)

  // minimal attribute set
  reaction.id = id
  reaction.receiver = receiver
  reaction.triggers = triggers

  // optional properties
  if (supplements && supplements.length) reaction.supplements = supplements

  return reaction
}


function createOscillator (spec, evaluatorSelector) {

  // all entity properties
  let {
    id = uuid.v4(),
    receiver,
    dependencies,
    target
  } = spec

  // required properties
  if (receiver == null) {
    throw Error("receiver must be set on this oscillator " + spec)
  }

  const oscillator = createAction(spec, evaluatorSelector)

  // minimal attribute set
  oscillator.id = id
  oscillator.receiver = receiver


  // optional properties
  if (dependencies != null) oscillator.dependencies = dependencies
  if (target != null) oscillator.target = target

  return oscillator
}


// ===== Module interface =====

module.exports = {
  createEntity,
  createAction,
  createFactory,
  createReaction,
  createOscillator,
  DEFAULT_VALUE_TYPE
}
