var {getNameNamespaceFromEntityKey} = require('../type-utils')


function parseRequireString (es) {
  return es.trim().split(/\s+/)
}


function load (objectSpec, manager) {


  function getEntityIdsFromRequireString (requireString) {
    return parseRequireString(requireString)
      .map(getNameNamespaceFromEntityKey)
      .map(function([name, namespace]) {
        return manager.getEntityByNameNamespace(name, namespace)
          || manager.addEntity({ name, namespace })
      })
      .map(e => e.id)
  }


  var factories = {},
      reactions = {},
      oscillators = {}


  for (let entityKey in objectSpec) {
    let entitySpec = objectSpec[entityKey],
        [name, namespace] = getNameNamespaceFromEntityKey(entityKey),
        data = { name, namespace }

    if (entitySpec.value != null) {
      data.initialValue = entitySpec.value
    }

    let entity = manager.addEntity(data)

    if (entitySpec.init) {
      factories[entity.id] = entitySpec
    }

    if (entitySpec.reactions) {
      reactions[entity.id] = entitySpec
    }

    if (entitySpec.oscillate) {
      oscillators[entity.id] = entitySpec
    }
  }

  for (let id in factories) {
    let entitySpec = factories[id]

    if (entitySpec.init.require) {

      let depIds = getEntityIdsFromRequireString(entitySpec.init.require)

      manager.addFactory({
        receiver: id,
        procedure: entitySpec.init["do"],
        dependencies: depIds
      })

    } else {

      manager.addFactory({
        receiver: id,
        procedure: entitySpec.init
      })
    }
  }

  for (let id in oscillators) {
    let entitySpec = oscillators[id],
        targetId = getEntityIdsFromRequireString(entitySpec.target)

    if (entitySpec.oscillate.require) {
      let depIds = getEntityIdsFromRequireString(entitySpec.oscillate.require)

      manager.addOscillator({
        receiver: id,
        target: targetId[0],
        procedure: entitySpec.oscillate["do"],
        dependencies: depIds
      })

    } else {

      manager.addOscillator({
        receiver: id,
        target: targetId[0],
        procedure: entitySpec.oscillate
      })
    }
  }

  for (let id in reactions) {
    let entitySpec = reactions[id]

    for (let triggerString in entitySpec.reactions) {
      let reactionSpec = entitySpec.reactions[triggerString],
          triggers = getEntityIdsFromRequireString(triggerString),
          receiver = id

      if (reactionSpec.require) {
        let supplements = getEntityIdsFromRequireString(reactionSpec.require)

        manager.addReaction({
          receiver: receiver,
          triggers: triggers,
          supplements: supplements,
          procedure: reactionSpec["do"]
        })

      } else {

        manager.addReaction({
          receiver: receiver,
          triggers: triggers,
          procedure: reactionSpec
        })
      }
    }
  }
}


module.exports = {
  load
}
