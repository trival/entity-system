var Types = require('./spec-types'),
    {update} = require('./utils/functional-helpers'),
    uniq = require('lodash/array/uniq')


function createSpec () {
  return {
    entities: {},
    factories: {},
    reactions: {},
    oscillators: {},
    metadata: {}
  }
}


function create (spec = createSpec()) {

  // ===== Spec transformations =====

  function addEntity (data) {
    const entity = Types.createEntity(data)
    spec = update(spec, { entities: { [entity.id]: { $set: entity }}})
    return entity
  }


  function addEntities (entities) {
    for (let i in entities) {
      addEntity(entities[i])
    }
  }


  function addFactory (data) {
    const factory = Types.createFactory(data),
          entity = spec.entities[factory.receiver]

    if (!entity) {
      throw Error("receiver '" + factory.receiver + "' not found for factory " + factory)
    }

    addEntity({...entity, factory: factory.id })
    spec = update(spec, { factories: { [factory.id]: { $set: factory }}})

    return factory
  }


  function addReaction (data) {
    const reaction = Types.createReaction(data),
          entity = spec.entities[reaction.receiver]

    if (!entity) {
      throw Error("receiver '" + reaction.receiver + "' not found for reaction " + reaction)
    }

    let entityReactions = entity.reactions || []
    entityReactions.push(reaction.id)
    entityReactions = uniq(entityReactions)

    addEntity({...entity, reactions: entityReactions })
    spec = update(spec, { reactions: { [reaction.id]: { $set: reaction }}})

    return reaction
  }


  function addOscillator (data) {
    const oscillator = Types.createOscillator(data),
          entity = spec.entities[oscillator.receiver]

    if (!entity) {
      throw Error("receiver '" + oscillator.receiver + "' not found for oscillator " + oscillator)
    }

    addEntity({ ...entity, oscillator: oscillator.id })
    spec = update(spec, { oscillators: { [oscillator.id]: { $set: oscillator }}})

    return oscillator
  }


  // ===== Runtime synchronisation =====

  function sendToRuntime (runtime) {

    for (let i in spec.entities) {
      runtime.addEntity(spec.entities[i])
    }

    for (let i in spec.factories) {
      runtime.addFactory(spec.factories[i])
    }

    for (let i in spec.oscillators) {
      runtime.addOscillator(spec.oscillators[i])
    }

    for (let i in spec.reactions) {
      runtime.addReaction(spec.reactions[i])
    }

    runtime.flush()
  }


  // ===== Queries =====

  function getSpec () { return spec }


  function getEntityById (id) {
    return spec.entities[id]
  }


  function getEntityByNameNamespace (name, namespace = '') {
    for (let id in spec.entities) {
      let entity = spec.entities[id]
      if (entity.name === name && entity.namespace === namespace) {
        return entity
      }
    }
  }


  // ===== Manager instance interface =====

  return {
    addEntity,
    addEntities,
    addFactory,
    addReaction,
    addOscillator,

    sendToRuntime,

    getSpec,
    getEntityById,
    getEntityByNameNamespace
  }
}


// ===== Module interface =====

module.exports = {
  create,
  createSpec
}

