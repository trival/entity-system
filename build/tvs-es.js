(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["tsEntitySystem"] = factory();
	else
		root["tsEntitySystem"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var RuntimeTypes = __webpack_require__(1),
	    Runtime = __webpack_require__(4),
	    SpecManager = __webpack_require__(5),
	    loaders = __webpack_require__(66),
	    utils = __webpack_require__(69);

	module.exports = {
	  RuntimeTypes: RuntimeTypes,
	  Runtime: Runtime,
	  SpecManager: SpecManager,
	  loaders: loaders,
	  utils: utils
	};

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

	var uuid = __webpack_require__(2);

	// ===== entity system types =====

	var ActionTypes = {
	  ACTION: "ts.entitysystem/action",
	  FACTORY: "ts.entitysystem/factory",
	  REACTION: "ts.entitysystem/reaction",
	  CALLBACK: "ts.entitysystem/callback",
	  OSCILLATOR: 'ts.entitysystem/oscillator'
	};

	function createEntity(_ref) {
	  var _ref$id = _ref.id;
	  var id = _ref$id === undefined ? uuid.v4() : _ref$id;
	  var name = _ref.name;
	  var namespace = _ref.namespace;
	  var value = _ref.initialValue;

	  return {
	    id: id,
	    value: value,
	    name: name,
	    namespace: namespace,
	    factory: null,
	    dependencies: null,
	    effects: null,
	    callbacks: null
	  };
	}

	function createAction(_ref2) {
	  var _ref2$id = _ref2.id;
	  var id = _ref2$id === undefined ? uuid.v4() : _ref2$id;
	  var dependencies = _ref2.dependencies;
	  var procedure = _ref2.procedure;

	  return {
	    id: id,
	    procedure: procedure,
	    dependencies: dependencies,
	    type: ActionTypes.ACTION
	  };
	}

	function createFactory(_ref3) {
	  var _ref3$id = _ref3.id;
	  var id = _ref3$id === undefined ? uuid.v4() : _ref3$id;
	  var _ref3$receiver = _ref3.receiver;
	  var receiver = _ref3$receiver === undefined ? uuid.v4() : _ref3$receiver;
	  var _ref3$dependencies = _ref3.dependencies;
	  var dependencies = _ref3$dependencies === undefined ? [] : _ref3$dependencies;
	  var procedure = _ref3.procedure;

	  return {
	    id: id,
	    receiver: receiver,
	    procedure: procedure,
	    dependencies: dependencies,
	    type: ActionTypes.FACTORY
	  };
	}

	function createReaction(_ref4) {
	  var _ref4$id = _ref4.id;
	  var id = _ref4$id === undefined ? uuid.v4() : _ref4$id;
	  var _ref4$receiver = _ref4.receiver;
	  var receiver = _ref4$receiver === undefined ? uuid.v4() : _ref4$receiver;
	  var triggers = _ref4.triggers;
	  var _ref4$supplements = _ref4.supplements;
	  var supplements = _ref4$supplements === undefined ? [] : _ref4$supplements;
	  var procedure = _ref4.procedure;

	  var dependencies = [receiver].concat(_toConsumableArray(triggers), _toConsumableArray(supplements));

	  return {
	    id: id,
	    receiver: receiver,
	    procedure: procedure,
	    dependencies: dependencies,
	    triggers: triggers,
	    supplements: supplements,
	    type: ActionTypes.REACTION
	  };
	}

	function createCallback(_ref5) {
	  var _ref5$id = _ref5.id;
	  var id = _ref5$id === undefined ? uuid.v4() : _ref5$id;
	  var triggers = _ref5.triggers;
	  var procedure = _ref5.procedure;
	  var _ref5$supplements = _ref5.supplements;
	  var supplements = _ref5$supplements === undefined ? [] : _ref5$supplements;

	  var dependencies = [].concat(_toConsumableArray(triggers), _toConsumableArray(supplements));

	  return {
	    id: id,
	    procedure: procedure,
	    dependencies: dependencies,
	    triggers: triggers,
	    supplements: supplements,
	    type: ActionTypes.CALLBACK
	  };
	}

	function createOscillator(_ref6) {
	  var _ref6$id = _ref6.id;
	  var id = _ref6$id === undefined ? uuid.v4() : _ref6$id;
	  var _ref6$receiver = _ref6.receiver;
	  var receiver = _ref6$receiver === undefined ? uuid.v4() : _ref6$receiver;
	  var _ref6$target = _ref6.target;
	  var target = _ref6$target === undefined ? uuid.v4() : _ref6$target;
	  var _ref6$dependencies = _ref6.dependencies;
	  var dependencies = _ref6$dependencies === undefined ? [] : _ref6$dependencies;
	  var procedure = _ref6.procedure;

	  return {
	    id: id,
	    receiver: receiver,
	    target: target,
	    procedure: procedure,
	    dependencies: dependencies,
	    type: ActionTypes.OSCILLATOR
	  };
	}

	// ===== module interface =====

	module.exports = {
	  ActionTypes: ActionTypes,
	  createEntity: createEntity,
	  createAction: createAction,
	  createFactory: createFactory,
	  createReaction: createReaction,
	  createCallback: createCallback,
	  createOscillator: createOscillator
	};

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	//     uuid.js
	//
	//     Copyright (c) 2010-2012 Robert Kieffer
	//     MIT License - http://opensource.org/licenses/mit-license.php

	// Unique ID creation requires a high quality random # generator.  We feature
	// detect to determine the best RNG source, normalizing to a function that
	// returns 128-bits of randomness, since that's what's usually required
	var _rng = __webpack_require__(3);

	// Maps for number <-> hex string conversion
	var _byteToHex = [];
	var _hexToByte = {};
	for (var i = 0; i < 256; i++) {
	  _byteToHex[i] = (i + 0x100).toString(16).substr(1);
	  _hexToByte[_byteToHex[i]] = i;
	}

	// **`parse()` - Parse a UUID into it's component bytes**
	function parse(s, buf, offset) {
	  var i = (buf && offset) || 0, ii = 0;

	  buf = buf || [];
	  s.toLowerCase().replace(/[0-9a-f]{2}/g, function(oct) {
	    if (ii < 16) { // Don't overflow!
	      buf[i + ii++] = _hexToByte[oct];
	    }
	  });

	  // Zero out remaining bytes if string was short
	  while (ii < 16) {
	    buf[i + ii++] = 0;
	  }

	  return buf;
	}

	// **`unparse()` - Convert UUID byte array (ala parse()) into a string**
	function unparse(buf, offset) {
	  var i = offset || 0, bth = _byteToHex;
	  return  bth[buf[i++]] + bth[buf[i++]] +
	          bth[buf[i++]] + bth[buf[i++]] + '-' +
	          bth[buf[i++]] + bth[buf[i++]] + '-' +
	          bth[buf[i++]] + bth[buf[i++]] + '-' +
	          bth[buf[i++]] + bth[buf[i++]] + '-' +
	          bth[buf[i++]] + bth[buf[i++]] +
	          bth[buf[i++]] + bth[buf[i++]] +
	          bth[buf[i++]] + bth[buf[i++]];
	}

	// **`v1()` - Generate time-based UUID**
	//
	// Inspired by https://github.com/LiosK/UUID.js
	// and http://docs.python.org/library/uuid.html

	// random #'s we need to init node and clockseq
	var _seedBytes = _rng();

	// Per 4.5, create and 48-bit node id, (47 random bits + multicast bit = 1)
	var _nodeId = [
	  _seedBytes[0] | 0x01,
	  _seedBytes[1], _seedBytes[2], _seedBytes[3], _seedBytes[4], _seedBytes[5]
	];

	// Per 4.2.2, randomize (14 bit) clockseq
	var _clockseq = (_seedBytes[6] << 8 | _seedBytes[7]) & 0x3fff;

	// Previous uuid creation time
	var _lastMSecs = 0, _lastNSecs = 0;

	// See https://github.com/broofa/node-uuid for API details
	function v1(options, buf, offset) {
	  var i = buf && offset || 0;
	  var b = buf || [];

	  options = options || {};

	  var clockseq = options.clockseq !== undefined ? options.clockseq : _clockseq;

	  // UUID timestamps are 100 nano-second units since the Gregorian epoch,
	  // (1582-10-15 00:00).  JSNumbers aren't precise enough for this, so
	  // time is handled internally as 'msecs' (integer milliseconds) and 'nsecs'
	  // (100-nanoseconds offset from msecs) since unix epoch, 1970-01-01 00:00.
	  var msecs = options.msecs !== undefined ? options.msecs : new Date().getTime();

	  // Per 4.2.1.2, use count of uuid's generated during the current clock
	  // cycle to simulate higher resolution clock
	  var nsecs = options.nsecs !== undefined ? options.nsecs : _lastNSecs + 1;

	  // Time since last uuid creation (in msecs)
	  var dt = (msecs - _lastMSecs) + (nsecs - _lastNSecs)/10000;

	  // Per 4.2.1.2, Bump clockseq on clock regression
	  if (dt < 0 && options.clockseq === undefined) {
	    clockseq = clockseq + 1 & 0x3fff;
	  }

	  // Reset nsecs if clock regresses (new clockseq) or we've moved onto a new
	  // time interval
	  if ((dt < 0 || msecs > _lastMSecs) && options.nsecs === undefined) {
	    nsecs = 0;
	  }

	  // Per 4.2.1.2 Throw error if too many uuids are requested
	  if (nsecs >= 10000) {
	    throw new Error('uuid.v1(): Can\'t create more than 10M uuids/sec');
	  }

	  _lastMSecs = msecs;
	  _lastNSecs = nsecs;
	  _clockseq = clockseq;

	  // Per 4.1.4 - Convert from unix epoch to Gregorian epoch
	  msecs += 12219292800000;

	  // `time_low`
	  var tl = ((msecs & 0xfffffff) * 10000 + nsecs) % 0x100000000;
	  b[i++] = tl >>> 24 & 0xff;
	  b[i++] = tl >>> 16 & 0xff;
	  b[i++] = tl >>> 8 & 0xff;
	  b[i++] = tl & 0xff;

	  // `time_mid`
	  var tmh = (msecs / 0x100000000 * 10000) & 0xfffffff;
	  b[i++] = tmh >>> 8 & 0xff;
	  b[i++] = tmh & 0xff;

	  // `time_high_and_version`
	  b[i++] = tmh >>> 24 & 0xf | 0x10; // include version
	  b[i++] = tmh >>> 16 & 0xff;

	  // `clock_seq_hi_and_reserved` (Per 4.2.2 - include variant)
	  b[i++] = clockseq >>> 8 | 0x80;

	  // `clock_seq_low`
	  b[i++] = clockseq & 0xff;

	  // `node`
	  var node = options.node || _nodeId;
	  for (var n = 0; n < 6; n++) {
	    b[i + n] = node[n];
	  }

	  return buf ? buf : unparse(b);
	}

	// **`v4()` - Generate random UUID**

	// See https://github.com/broofa/node-uuid for API details
	function v4(options, buf, offset) {
	  // Deprecated - 'format' argument, as supported in v1.2
	  var i = buf && offset || 0;

	  if (typeof(options) == 'string') {
	    buf = options == 'binary' ? new Array(16) : null;
	    options = null;
	  }
	  options = options || {};

	  var rnds = options.random || (options.rng || _rng)();

	  // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`
	  rnds[6] = (rnds[6] & 0x0f) | 0x40;
	  rnds[8] = (rnds[8] & 0x3f) | 0x80;

	  // Copy bytes to buffer, if provided
	  if (buf) {
	    for (var ii = 0; ii < 16; ii++) {
	      buf[i + ii] = rnds[ii];
	    }
	  }

	  return buf || unparse(rnds);
	}

	// Export public API
	var uuid = v4;
	uuid.v1 = v1;
	uuid.v4 = v4;
	uuid.parse = parse;
	uuid.unparse = unparse;

	module.exports = uuid;


/***/ },
/* 3 */
/***/ function(module, exports) {

	/* WEBPACK VAR INJECTION */(function(global) {
	var rng;

	if (global.crypto && crypto.getRandomValues) {
	  // WHATWG crypto-based RNG - http://wiki.whatwg.org/wiki/Crypto
	  // Moderately fast, high quality
	  var _rnds8 = new Uint8Array(16);
	  rng = function whatwgRNG() {
	    crypto.getRandomValues(_rnds8);
	    return _rnds8;
	  };
	}

	if (!rng) {
	  // Math.random()-based (RNG)
	  //
	  // If all else fails, use Math.random().  It's fast, but is of unspecified
	  // quality.
	  var  _rnds = new Array(16);
	  rng = function() {
	    for (var i = 0, r; i < 16; i++) {
	      if ((i & 0x03) === 0) r = Math.random() * 0x100000000;
	      _rnds[i] = r >>> ((i & 0x03) << 3) & 0xff;
	    }

	    return _rnds;
	  };
	}

	module.exports = rng;


	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var types = __webpack_require__(1);

	// ===== runtime factory =====

	// Generates an empty entity system runtime.
	module.exports.create = function () {

	  // ===== permanent state =====

	  // All entity data.
	  var entities = {};

	  // All actions operating on the entities.
	  var actions = {};

	  // The context that is accessible in action proceures
	  var context = null;

	  // ===== temporal state =====

	  // Stores all changes and processes them at flush.
	  // Empty after each flush.
	  var changes = {};

	  // Callbacks executed after changes are processed.
	  // Empty after each flush.
	  var calls = {};

	  function getState() {
	    return { entities: entities, actions: actions, changes: changes, calls: calls };
	  }

	  function getContext() {
	    return context;
	  }

	  function setContext(ctx) {
	    context = ctx;
	  }

	  // ===== system topology updates =====

	  function addEntity() {
	    var entity = types.createEntity.apply(types, arguments);
	    return entities[entity.id] = entity;
	  }

	  function removeEntity(eid) {
	    throw new Error('remove Entity not yet implemented');
	  }

	  function addFactory() {
	    var factory = types.createFactory.apply(types, arguments);
	    setupAction(factory);
	    var entity = getOrCreateEntity(factory.receiver);
	    if (factory.dependencies.length) {
	      entity.factory = factory.id;
	      updateEntityReactions(entity, factory.id, factory.dependencies);
	    }
	    entity.value = executeAction(factory);
	    propagateChange(entity);
	    return factory;
	  }

	  function removeFactory(eid) {
	    throw new Error('remove Factory not yet implemented');
	  }

	  function addReaction() {
	    var reaction = types.createReaction.apply(types, arguments);
	    setupAction(reaction);
	    var entity = getOrCreateEntity(reaction.receiver);
	    updateEntityReactions(entity, reaction.id, reaction.triggers);
	    for (var depid in entity.dependencies) {
	      touch(depid);
	    }
	    return reaction;
	  }

	  function removeReaction(eid) {
	    throw new Error('remove Reaction not yet implemented');
	  }

	  function updateEntityReactions(entity, actionId, dependencies) {
	    entity.dependencies = entity.dependencies || {};

	    for (var i in dependencies) {
	      var depid = dependencies[i];
	      entity.dependencies[depid] = actionId;
	      var depEntity = getOrCreateEntity(depid);
	      depEntity.effects = depEntity.effects || {};
	      depEntity.effects[entity.id] = actionId;
	    }
	  }

	  function addAction() {
	    return setupAction(types.createAction.apply(types, arguments));
	  }

	  function removeAction(eid) {
	    throw new Error('remove Action not yet implemented');
	  }

	  function addCallback() {
	    var callback = types.createCallback.apply(types, arguments);
	    setupAction(callback);
	    for (var i in callback.triggers) {
	      var entity = getOrCreateEntity(callback.triggers[i]);
	      entity.callbacks = entity.callbacks || [];
	      if (entity.callbacks.indexOf(callback.id) < 0) {
	        entity.callbacks.push(callback.id);
	      }
	    }
	    return callback;
	  }

	  function removeCallback(cid) {
	    var deps = actions[cid].triggers;
	    for (var i in deps) {
	      var e = entities[deps[i]];
	      e.callbacks = e.callbacks.filter(function (cb) {
	        return cb !== cid;
	      });
	    }
	    delete calls[cid];
	    delete actions[cid];
	  }

	  function addOscillator() {
	    var oscillator = types.createOscillator.apply(types, arguments);
	    setupAction(oscillator);
	    var receiver = getOrCreateEntity(oscillator.receiver);
	    receiver.oscillator = oscillator.id;
	    oscillator.sink = function (val, doFlush) {
	      set(oscillator.target, val);
	      if (doFlush) flush();
	    };

	    if (oscillator.dependencies.length) {
	      updateEntityReactions(receiver, oscillator.id, oscillator.dependencies);
	    }

	    receiver.value = executeAction(oscillator);
	    return oscillator;
	  }

	  function removeOscillator() {}

	  function setupAction(action) {
	    return actions[action.id] = action;
	  }

	  // ===== interacting with entities =====

	  function get(eid) {
	    if (entities[eid] != null) return entities[eid].value;
	  }

	  function set(eid, val) {
	    var entity = getOrCreateEntity(eid);
	    entity.value = val;
	    propagateChange(entity);
	  }

	  function update(eid, fn) {
	    set(eid, fn(get(eid)));
	  }

	  function touch(eid) {
	    propagateChange(entities[eid]);
	  }

	  function getOrCreateEntity(eid) {
	    return entities[eid] || addEntity({ id: eid });
	  }

	  function getEntity(eid) {
	    return entities[eid];
	  }

	  // ===== change management =====

	  function updateChange(entity, aid, order) {
	    var change = changes[aid] != null ? changes[aid] : changes[aid] = {
	      order: order,
	      entity: entity
	    };
	    if (change.order < order) {
	      change.order = order;
	    }
	  }

	  function propagateChange(entity, order) {
	    if (entity.effects != null) {
	      order = order || 1;

	      for (var eid in entity.effects) {
	        var aid = entity.effects[eid],
	            nextEntity = entities[eid];
	        updateChange(nextEntity, aid, order);

	        // restore reactions state after reinitialization
	        if (actions[aid].type === types.ActionTypes.FACTORY) {
	          for (var i in nextEntity.dependencies) {
	            updateChange(nextEntity, nextEntity.dependencies[i], order);
	          }
	        }

	        propagateChange(nextEntity, order + 1);
	      }
	    }

	    // handle callbacks
	    if (entity.callbacks && entity.callbacks.length) {
	      for (var i = 0; i < entity.callbacks.length; i++) {
	        calls[entity.callbacks[i]] = true;
	      }
	    }
	  }

	  function flush() {
	    var process = {};
	    for (var aid in changes) {
	      var change = changes[aid],
	          action = actions[aid],
	          steps = process[change.order] || (process[change.order] = []),
	          unit = {
	        action: action,
	        entity: change.entity
	      };
	      if (action.type === types.ActionTypes.FACTORY) {
	        steps.unshift(unit);
	      } else {
	        steps.push(unit);
	      }
	    }

	    changes = {};

	    for (var order in process) {
	      var steps = process[order];
	      for (var i = 0; i < steps.length; i++) {
	        var _steps$i = steps[i];
	        var action = _steps$i.action;
	        var entity = _steps$i.entity;
	        var res = executeAction(action);
	        if (res != null) {
	          entity.value = res;
	        } else if (res === null) {
	          console.warn('TODO: implement propagation stop!');
	        }
	      }
	    }

	    for (var cid in calls) {
	      executeAction(actions[cid]);
	    }

	    calls = {};
	  }

	  function executeAction(action) {
	    var args = [];

	    for (var i = 0; i < action.dependencies.length; i++) {
	      args[i] = entities[action.dependencies[i]].value;
	    }

	    // handle oscillators
	    if (action.sink) {
	      var osc = get(action.receiver);
	      if (osc && typeof osc === 'function') osc();
	      args.unshift(action.sink);
	    }

	    return action.procedure.apply(context, args);
	  }

	  function callAction(actionId) {
	    return executeAction(actions[actionId]);
	  }

	  // ===== system interface =====

	  return {
	    get: get,
	    set: set,
	    touch: touch,
	    update: update,

	    addEntity: addEntity,
	    removeEntity: removeEntity,
	    addFactory: addFactory,
	    removeFactory: removeFactory,
	    addReaction: addReaction,
	    removeReaction: removeReaction,
	    addAction: addAction,
	    removeAction: removeAction,
	    addCallback: addCallback,
	    removeCallback: removeCallback,
	    addOscillator: addOscillator,
	    removeOscillator: removeOscillator,

	    flush: flush,
	    callAction: callAction,

	    getEntity: getEntity,
	    getState: getState,

	    getContext: getContext,
	    setContext: setContext
	  };
	};

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

	var Types = __webpack_require__(6);

	var _require = __webpack_require__(8);

	var update = _require.update;
	var uniq = __webpack_require__(16);

	function createSpec() {
	  return {
	    entities: {},
	    factories: {},
	    reactions: {},
	    oscillators: {},
	    metadata: {}
	  };
	}

	function create() {
	  var spec = arguments.length <= 0 || arguments[0] === undefined ? createSpec() : arguments[0];

	  // ===== Spec transformations =====

	  function addEntity(data) {
	    var entity = Types.createEntity(data);
	    spec = update(spec, { entities: _defineProperty({}, entity.id, { $set: entity }) });
	    return entity;
	  }

	  function addEntities(entities) {
	    for (var i in entities) {
	      addEntity(entities[i]);
	    }
	  }

	  function addFactory(data) {
	    var factory = Types.createFactory(data),
	        entity = spec.entities[factory.receiver];

	    if (!entity) {
	      throw Error("receiver '" + factory.receiver + "' not found for factory " + factory);
	    }

	    addEntity(_extends({}, entity, { factory: factory.id }));
	    spec = update(spec, { factories: _defineProperty({}, factory.id, { $set: factory }) });

	    return factory;
	  }

	  function addReaction(data) {
	    var reaction = Types.createReaction(data),
	        entity = spec.entities[reaction.receiver];

	    if (!entity) {
	      throw Error("receiver '" + reaction.receiver + "' not found for reaction " + reaction);
	    }

	    var entityReactions = entity.reactions || [];
	    entityReactions.push(reaction.id);
	    entityReactions = uniq(entityReactions);

	    addEntity(_extends({}, entity, { reactions: entityReactions }));
	    spec = update(spec, { reactions: _defineProperty({}, reaction.id, { $set: reaction }) });

	    return reaction;
	  }

	  function addOscillator(data) {
	    var oscillator = Types.createOscillator(data),
	        entity = spec.entities[oscillator.receiver];

	    if (!entity) {
	      throw Error("receiver '" + oscillator.receiver + "' not found for oscillator " + oscillator);
	    }

	    addEntity(_extends({}, entity, { oscillator: oscillator.id }));
	    spec = update(spec, { oscillators: _defineProperty({}, oscillator.id, { $set: oscillator }) });

	    return oscillator;
	  }

	  // ===== Runtime synchronisation =====

	  function sendToRuntime(runtime) {

	    for (var i in spec.entities) {
	      runtime.addEntity(spec.entities[i]);
	    }

	    for (var i in spec.factories) {
	      runtime.addFactory(spec.factories[i]);
	    }

	    for (var i in spec.oscillators) {
	      runtime.addOscillator(spec.oscillators[i]);
	    }

	    for (var i in spec.reactions) {
	      runtime.addReaction(spec.reactions[i]);
	    }

	    runtime.flush();
	  }

	  // ===== Queries =====

	  function getSpec() {
	    return spec;
	  }

	  function getEntityById(id) {
	    return spec.entities[id];
	  }

	  function getEntityByNameNamespace(name) {
	    var namespace = arguments.length <= 1 || arguments[1] === undefined ? '' : arguments[1];

	    for (var id in spec.entities) {
	      var entity = spec.entities[id];
	      if (entity.name === name && entity.namespace === namespace) {
	        return entity;
	      }
	    }
	  }

	  // ===== Manager instance interface =====

	  return {
	    addEntity: addEntity,
	    addEntities: addEntities,
	    addFactory: addFactory,
	    addReaction: addReaction,
	    addOscillator: addOscillator,

	    sendToRuntime: sendToRuntime,

	    getSpec: getSpec,
	    getEntityById: getEntityById,
	    getEntityByNameNamespace: getEntityByNameNamespace
	  };
	}

	// ===== Module interface =====

	module.exports = {
	  create: create,
	  createSpec: createSpec
	};

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var uuid = __webpack_require__(2),
	    Evaluator = __webpack_require__(7);

	// ===== Defaults =====

	var DEFAULT_VALUE_TYPE = "JSON";

	// ===== Entity creation =====

	function createEntity(spec) {

	  // all entity properties
	  var _spec$id = spec.id;
	  var id = _spec$id === undefined ? uuid.v4() : _spec$id;
	  var name = spec.name;
	  var _spec$namespace = spec.namespace;
	  var namespace = _spec$namespace === undefined ? '' : _spec$namespace;
	  var _spec$valueType = spec.valueType;
	  var valueType = _spec$valueType === undefined ? DEFAULT_VALUE_TYPE : _spec$valueType;
	  var initialValue = spec.initialValue;
	  var factory = spec.factory;
	  var reactions = spec.reactions;
	  var oscillator = spec.oscillator;

	  // required properties
	  if (name == null) {
	    throw Error("name must be set on this entity " + spec);
	  }

	  // minimal attribute set
	  var entity = {
	    id: id,
	    name: name,
	    namespace: namespace,
	    valueType: valueType
	  };

	  // optional attributes
	  if (initialValue != null) entity.initialValue = initialValue;
	  if (factory != null) entity.factory = factory;
	  if (oscillator != null) entity.oscillator = oscillator;
	  if (reactions != null) entity.reactions = reactions;

	  return entity;
	}

	function createAction(spec, evaluatorSelector) {

	  // all entity properties
	  var procedure = spec.procedure;
	  var code = spec.code;
	  var language = spec.language;

	  // required properties
	  if (!(procedure != null || code != null)) {
	    throw Error("procedure or code property must be set on this action " + spec);
	  }

	  // calculated values
	  if (code == null) code = procedure.toString();
	  if (procedure == null) {
	    var evaluate = evaluatorSelector && evaluatorSelector(language) || Evaluator.evaluate;
	    procedure = evaluate(code);
	  }

	  // predicates
	  if (typeof procedure !== 'function') {
	    throw Error("procedure must be a valid functions on this action " + spec);
	  }

	  // minimal attribute set
	  var action = {
	    procedure: procedure,
	    code: code
	  };

	  // optional properties
	  if (language != null) action.language = language;

	  return action;
	}

	function createFactory(spec, evaluatorSelector) {

	  // all entity properties
	  var _spec$id2 = spec.id;
	  var id = _spec$id2 === undefined ? uuid.v4() : _spec$id2;
	  var receiver = spec.receiver;
	  var dependencies = spec.dependencies;

	  // required properties
	  if (receiver == null) {
	    throw Error("receiver must be set on this factory " + spec);
	  }

	  var factory = createAction(spec, evaluatorSelector);

	  // minimal attribute set
	  factory.id = id;
	  factory.receiver = receiver;

	  // optional properties
	  if (dependencies != null) factory.dependencies = dependencies;

	  return factory;
	}

	function createReaction(spec, evaluatorSelector) {

	  // all entity properties
	  var _spec$id3 = spec.id;
	  var id = _spec$id3 === undefined ? uuid.v4() : _spec$id3;
	  var receiver = spec.receiver;
	  var triggers = spec.triggers;
	  var supplements = spec.supplements;

	  // required properties
	  if (receiver == null) {
	    throw Error("receiver must be set on this reaction " + spec);
	  }
	  if (!(triggers && triggers.length)) {
	    throw Error("triggers must have at least one element on this reaction " + spec);
	  }

	  var reaction = createAction(spec, evaluatorSelector);

	  // minimal attribute set
	  reaction.id = id;
	  reaction.receiver = receiver;
	  reaction.triggers = triggers;

	  // optional properties
	  if (supplements && supplements.length) reaction.supplements = supplements;

	  return reaction;
	}

	function createOscillator(spec, evaluatorSelector) {

	  // all entity properties
	  var _spec$id4 = spec.id;
	  var id = _spec$id4 === undefined ? uuid.v4() : _spec$id4;
	  var receiver = spec.receiver;
	  var dependencies = spec.dependencies;
	  var target = spec.target;

	  // required properties
	  if (receiver == null) {
	    throw Error("receiver must be set on this oscillator " + spec);
	  }

	  var oscillator = createAction(spec, evaluatorSelector);

	  // minimal attribute set
	  oscillator.id = id;
	  oscillator.receiver = receiver;

	  // optional properties
	  if (dependencies != null) oscillator.dependencies = dependencies;
	  if (target != null) oscillator.target = target;

	  return oscillator;
	}

	// ===== Module interface =====

	module.exports = {
	  createEntity: createEntity,
	  createAction: createAction,
	  createFactory: createFactory,
	  createReaction: createReaction,
	  createOscillator: createOscillator,
	  DEFAULT_VALUE_TYPE: DEFAULT_VALUE_TYPE
	};

/***/ },
/* 7 */
/***/ function(module, exports) {

	"use strict";

	function evaluate(code, context) {
	  var prefix = "(function(){ return ";
	  var postfix = "})";
	  var factory = eval(prefix + code + postfix);
	  return factory.call(context);
	}

	module.exports = {
	  evaluate: evaluate
	};

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var assign = __webpack_require__(9),
	    update = __webpack_require__(10);

	function extend() {
	  var args = [{}];
	  for (var i = 0, len = arguments.length; i < len; i++) {
	    args.push(arguments[i]);
	  }
	  return assign.apply(null, args);
	}

	module.exports = {
	  extend: extend,
	  assign: assign,
	  update: update
	};

/***/ },
/* 9 */
/***/ function(module, exports) {

	'use strict';
	var propIsEnumerable = Object.prototype.propertyIsEnumerable;

	function ToObject(val) {
		if (val == null) {
			throw new TypeError('Object.assign cannot be called with null or undefined');
		}

		return Object(val);
	}

	function ownEnumerableKeys(obj) {
		var keys = Object.getOwnPropertyNames(obj);

		if (Object.getOwnPropertySymbols) {
			keys = keys.concat(Object.getOwnPropertySymbols(obj));
		}

		return keys.filter(function (key) {
			return propIsEnumerable.call(obj, key);
		});
	}

	module.exports = Object.assign || function (target, source) {
		var from;
		var keys;
		var to = ToObject(target);

		for (var s = 1; s < arguments.length; s++) {
			from = arguments[s];
			keys = ownEnumerableKeys(Object(from));

			for (var i = 0; i < keys.length; i++) {
				to[keys[i]] = from[keys[i]];
			}
		}

		return to;
	};


/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(11);

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {/**
	 * Copyright 2013-2015, Facebook, Inc.
	 * All rights reserved.
	 *
	 * This source code is licensed under the BSD-style license found in the
	 * LICENSE file in the root directory of this source tree. An additional grant
	 * of patent rights can be found in the PATENTS file in the same directory.
	 *
	 * @providesModule update
	 */

	/* global hasOwnProperty:true */

	'use strict';

	var assign = __webpack_require__(13);
	var keyOf = __webpack_require__(14);
	var invariant = __webpack_require__(15);
	var hasOwnProperty = ({}).hasOwnProperty;

	function shallowCopy(x) {
	  if (Array.isArray(x)) {
	    return x.concat();
	  } else if (x && typeof x === 'object') {
	    return assign(new x.constructor(), x);
	  } else {
	    return x;
	  }
	}

	var COMMAND_PUSH = keyOf({ $push: null });
	var COMMAND_UNSHIFT = keyOf({ $unshift: null });
	var COMMAND_SPLICE = keyOf({ $splice: null });
	var COMMAND_SET = keyOf({ $set: null });
	var COMMAND_MERGE = keyOf({ $merge: null });
	var COMMAND_APPLY = keyOf({ $apply: null });

	var ALL_COMMANDS_LIST = [COMMAND_PUSH, COMMAND_UNSHIFT, COMMAND_SPLICE, COMMAND_SET, COMMAND_MERGE, COMMAND_APPLY];

	var ALL_COMMANDS_SET = {};

	ALL_COMMANDS_LIST.forEach(function (command) {
	  ALL_COMMANDS_SET[command] = true;
	});

	function invariantArrayCase(value, spec, command) {
	  !Array.isArray(value) ? process.env.NODE_ENV !== 'production' ? invariant(false, 'update(): expected target of %s to be an array; got %s.', command, value) : invariant(false) : undefined;
	  var specValue = spec[command];
	  !Array.isArray(specValue) ? process.env.NODE_ENV !== 'production' ? invariant(false, 'update(): expected spec of %s to be an array; got %s. ' + 'Did you forget to wrap your parameter in an array?', command, specValue) : invariant(false) : undefined;
	}

	function update(value, spec) {
	  !(typeof spec === 'object') ? process.env.NODE_ENV !== 'production' ? invariant(false, 'update(): You provided a key path to update() that did not contain one ' + 'of %s. Did you forget to include {%s: ...}?', ALL_COMMANDS_LIST.join(', '), COMMAND_SET) : invariant(false) : undefined;

	  if (hasOwnProperty.call(spec, COMMAND_SET)) {
	    !(Object.keys(spec).length === 1) ? process.env.NODE_ENV !== 'production' ? invariant(false, 'Cannot have more than one key in an object with %s', COMMAND_SET) : invariant(false) : undefined;

	    return spec[COMMAND_SET];
	  }

	  var nextValue = shallowCopy(value);

	  if (hasOwnProperty.call(spec, COMMAND_MERGE)) {
	    var mergeObj = spec[COMMAND_MERGE];
	    !(mergeObj && typeof mergeObj === 'object') ? process.env.NODE_ENV !== 'production' ? invariant(false, 'update(): %s expects a spec of type \'object\'; got %s', COMMAND_MERGE, mergeObj) : invariant(false) : undefined;
	    !(nextValue && typeof nextValue === 'object') ? process.env.NODE_ENV !== 'production' ? invariant(false, 'update(): %s expects a target of type \'object\'; got %s', COMMAND_MERGE, nextValue) : invariant(false) : undefined;
	    assign(nextValue, spec[COMMAND_MERGE]);
	  }

	  if (hasOwnProperty.call(spec, COMMAND_PUSH)) {
	    invariantArrayCase(value, spec, COMMAND_PUSH);
	    spec[COMMAND_PUSH].forEach(function (item) {
	      nextValue.push(item);
	    });
	  }

	  if (hasOwnProperty.call(spec, COMMAND_UNSHIFT)) {
	    invariantArrayCase(value, spec, COMMAND_UNSHIFT);
	    spec[COMMAND_UNSHIFT].forEach(function (item) {
	      nextValue.unshift(item);
	    });
	  }

	  if (hasOwnProperty.call(spec, COMMAND_SPLICE)) {
	    !Array.isArray(value) ? process.env.NODE_ENV !== 'production' ? invariant(false, 'Expected %s target to be an array; got %s', COMMAND_SPLICE, value) : invariant(false) : undefined;
	    !Array.isArray(spec[COMMAND_SPLICE]) ? process.env.NODE_ENV !== 'production' ? invariant(false, 'update(): expected spec of %s to be an array of arrays; got %s. ' + 'Did you forget to wrap your parameters in an array?', COMMAND_SPLICE, spec[COMMAND_SPLICE]) : invariant(false) : undefined;
	    spec[COMMAND_SPLICE].forEach(function (args) {
	      !Array.isArray(args) ? process.env.NODE_ENV !== 'production' ? invariant(false, 'update(): expected spec of %s to be an array of arrays; got %s. ' + 'Did you forget to wrap your parameters in an array?', COMMAND_SPLICE, spec[COMMAND_SPLICE]) : invariant(false) : undefined;
	      nextValue.splice.apply(nextValue, args);
	    });
	  }

	  if (hasOwnProperty.call(spec, COMMAND_APPLY)) {
	    !(typeof spec[COMMAND_APPLY] === 'function') ? process.env.NODE_ENV !== 'production' ? invariant(false, 'update(): expected spec of %s to be a function; got %s.', COMMAND_APPLY, spec[COMMAND_APPLY]) : invariant(false) : undefined;
	    nextValue = spec[COMMAND_APPLY](nextValue);
	  }

	  for (var k in spec) {
	    if (!(ALL_COMMANDS_SET.hasOwnProperty(k) && ALL_COMMANDS_SET[k])) {
	      nextValue[k] = update(value[k], spec[k]);
	    }
	  }

	  return nextValue;
	}

	module.exports = update;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(12)))

/***/ },
/* 12 */
/***/ function(module, exports) {

	// shim for using process in browser

	var process = module.exports = {};
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;

	function cleanUpNextTick() {
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}

	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = setTimeout(cleanUpNextTick);
	    draining = true;

	    var len = queue.length;
	    while(len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    clearTimeout(timeout);
	}

	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        setTimeout(drainQueue, 0);
	    }
	};

	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};

	function noop() {}

	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;

	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};

	process.cwd = function () { return '/' };
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function() { return 0; };


/***/ },
/* 13 */
/***/ function(module, exports) {

	/**
	 * Copyright 2014-2015, Facebook, Inc.
	 * All rights reserved.
	 *
	 * This source code is licensed under the BSD-style license found in the
	 * LICENSE file in the root directory of this source tree. An additional grant
	 * of patent rights can be found in the PATENTS file in the same directory.
	 *
	 * @providesModule Object.assign
	 */

	// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-object.assign

	'use strict';

	function assign(target, sources) {
	  if (target == null) {
	    throw new TypeError('Object.assign target cannot be null or undefined');
	  }

	  var to = Object(target);
	  var hasOwnProperty = Object.prototype.hasOwnProperty;

	  for (var nextIndex = 1; nextIndex < arguments.length; nextIndex++) {
	    var nextSource = arguments[nextIndex];
	    if (nextSource == null) {
	      continue;
	    }

	    var from = Object(nextSource);

	    // We don't currently support accessors nor proxies. Therefore this
	    // copy cannot throw. If we ever supported this then we must handle
	    // exceptions and side-effects. We don't support symbols so they won't
	    // be transferred.

	    for (var key in from) {
	      if (hasOwnProperty.call(from, key)) {
	        to[key] = from[key];
	      }
	    }
	  }

	  return to;
	}

	module.exports = assign;

/***/ },
/* 14 */
/***/ function(module, exports) {

	/**
	 * Copyright 2013-2015, Facebook, Inc.
	 * All rights reserved.
	 *
	 * This source code is licensed under the BSD-style license found in the
	 * LICENSE file in the root directory of this source tree. An additional grant
	 * of patent rights can be found in the PATENTS file in the same directory.
	 *
	 * @providesModule keyOf
	 */

	/**
	 * Allows extraction of a minified key. Let's the build system minify keys
	 * without losing the ability to dynamically use key strings as values
	 * themselves. Pass in an object with a single key/val pair and it will return
	 * you the string key of that single record. Suppose you want to grab the
	 * value for a key 'className' inside of an object. Key/val minification may
	 * have aliased that key to be 'xa12'. keyOf({className: null}) will return
	 * 'xa12' in that case. Resolve keys you want to use once at startup time, then
	 * reuse those resolutions.
	 */
	"use strict";

	var keyOf = function (oneKeyObj) {
	  var key;
	  for (key in oneKeyObj) {
	    if (!oneKeyObj.hasOwnProperty(key)) {
	      continue;
	    }
	    return key;
	  }
	  return null;
	};

	module.exports = keyOf;

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(process) {/**
	 * Copyright 2013-2015, Facebook, Inc.
	 * All rights reserved.
	 *
	 * This source code is licensed under the BSD-style license found in the
	 * LICENSE file in the root directory of this source tree. An additional grant
	 * of patent rights can be found in the PATENTS file in the same directory.
	 *
	 * @providesModule invariant
	 */

	'use strict';

	/**
	 * Use invariant() to assert state which your program assumes to be true.
	 *
	 * Provide sprintf-style format (only %s is supported) and arguments
	 * to provide information about what broke and what you were
	 * expecting.
	 *
	 * The invariant message will be stripped in production, but the invariant
	 * will remain to ensure logic does not differ in production.
	 */

	var invariant = function (condition, format, a, b, c, d, e, f) {
	  if (process.env.NODE_ENV !== 'production') {
	    if (format === undefined) {
	      throw new Error('invariant requires an error message argument');
	    }
	  }

	  if (!condition) {
	    var error;
	    if (format === undefined) {
	      error = new Error('Minified exception occurred; use the non-minified dev environment ' + 'for the full error message and additional helpful warnings.');
	    } else {
	      var args = [a, b, c, d, e, f];
	      var argIndex = 0;
	      error = new Error('Invariant Violation: ' + format.replace(/%s/g, function () {
	        return args[argIndex++];
	      }));
	    }

	    error.framesToPop = 1; // we don't care about invariant's own frame
	    throw error;
	  }
	};

	module.exports = invariant;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(12)))

/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	var baseCallback = __webpack_require__(17),
	    baseUniq = __webpack_require__(57),
	    isIterateeCall = __webpack_require__(64),
	    sortedUniq = __webpack_require__(65);

	/**
	 * Creates a duplicate-free version of an array, using
	 * [`SameValueZero`](http://ecma-international.org/ecma-262/6.0/#sec-samevaluezero)
	 * for equality comparisons, in which only the first occurence of each element
	 * is kept. Providing `true` for `isSorted` performs a faster search algorithm
	 * for sorted arrays. If an iteratee function is provided it's invoked for
	 * each element in the array to generate the criterion by which uniqueness
	 * is computed. The `iteratee` is bound to `thisArg` and invoked with three
	 * arguments: (value, index, array).
	 *
	 * If a property name is provided for `iteratee` the created `_.property`
	 * style callback returns the property value of the given element.
	 *
	 * If a value is also provided for `thisArg` the created `_.matchesProperty`
	 * style callback returns `true` for elements that have a matching property
	 * value, else `false`.
	 *
	 * If an object is provided for `iteratee` the created `_.matches` style
	 * callback returns `true` for elements that have the properties of the given
	 * object, else `false`.
	 *
	 * @static
	 * @memberOf _
	 * @alias unique
	 * @category Array
	 * @param {Array} array The array to inspect.
	 * @param {boolean} [isSorted] Specify the array is sorted.
	 * @param {Function|Object|string} [iteratee] The function invoked per iteration.
	 * @param {*} [thisArg] The `this` binding of `iteratee`.
	 * @returns {Array} Returns the new duplicate-value-free array.
	 * @example
	 *
	 * _.uniq([2, 1, 2]);
	 * // => [2, 1]
	 *
	 * // using `isSorted`
	 * _.uniq([1, 1, 2], true);
	 * // => [1, 2]
	 *
	 * // using an iteratee function
	 * _.uniq([1, 2.5, 1.5, 2], function(n) {
	 *   return this.floor(n);
	 * }, Math);
	 * // => [1, 2.5]
	 *
	 * // using the `_.property` callback shorthand
	 * _.uniq([{ 'x': 1 }, { 'x': 2 }, { 'x': 1 }], 'x');
	 * // => [{ 'x': 1 }, { 'x': 2 }]
	 */
	function uniq(array, isSorted, iteratee, thisArg) {
	  var length = array ? array.length : 0;
	  if (!length) {
	    return [];
	  }
	  if (isSorted != null && typeof isSorted != 'boolean') {
	    thisArg = iteratee;
	    iteratee = isIterateeCall(array, isSorted, thisArg) ? undefined : isSorted;
	    isSorted = false;
	  }
	  iteratee = iteratee == null ? iteratee : baseCallback(iteratee, thisArg, 3);
	  return (isSorted)
	    ? sortedUniq(array, iteratee)
	    : baseUniq(array, iteratee);
	}

	module.exports = uniq;


/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	var baseMatches = __webpack_require__(18),
	    baseMatchesProperty = __webpack_require__(46),
	    bindCallback = __webpack_require__(53),
	    identity = __webpack_require__(54),
	    property = __webpack_require__(55);

	/**
	 * The base implementation of `_.callback` which supports specifying the
	 * number of arguments to provide to `func`.
	 *
	 * @private
	 * @param {*} [func=_.identity] The value to convert to a callback.
	 * @param {*} [thisArg] The `this` binding of `func`.
	 * @param {number} [argCount] The number of arguments to provide to `func`.
	 * @returns {Function} Returns the callback.
	 */
	function baseCallback(func, thisArg, argCount) {
	  var type = typeof func;
	  if (type == 'function') {
	    return thisArg === undefined
	      ? func
	      : bindCallback(func, thisArg, argCount);
	  }
	  if (func == null) {
	    return identity;
	  }
	  if (type == 'object') {
	    return baseMatches(func);
	  }
	  return thisArg === undefined
	    ? property(func)
	    : baseMatchesProperty(func, thisArg);
	}

	module.exports = baseCallback;


/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	var baseIsMatch = __webpack_require__(19),
	    getMatchData = __webpack_require__(43),
	    toObject = __webpack_require__(42);

	/**
	 * The base implementation of `_.matches` which does not clone `source`.
	 *
	 * @private
	 * @param {Object} source The object of property values to match.
	 * @returns {Function} Returns the new function.
	 */
	function baseMatches(source) {
	  var matchData = getMatchData(source);
	  if (matchData.length == 1 && matchData[0][2]) {
	    var key = matchData[0][0],
	        value = matchData[0][1];

	    return function(object) {
	      if (object == null) {
	        return false;
	      }
	      return object[key] === value && (value !== undefined || (key in toObject(object)));
	    };
	  }
	  return function(object) {
	    return baseIsMatch(object, matchData);
	  };
	}

	module.exports = baseMatches;


/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	var baseIsEqual = __webpack_require__(20),
	    toObject = __webpack_require__(42);

	/**
	 * The base implementation of `_.isMatch` without support for callback
	 * shorthands and `this` binding.
	 *
	 * @private
	 * @param {Object} object The object to inspect.
	 * @param {Array} matchData The propery names, values, and compare flags to match.
	 * @param {Function} [customizer] The function to customize comparing objects.
	 * @returns {boolean} Returns `true` if `object` is a match, else `false`.
	 */
	function baseIsMatch(object, matchData, customizer) {
	  var index = matchData.length,
	      length = index,
	      noCustomizer = !customizer;

	  if (object == null) {
	    return !length;
	  }
	  object = toObject(object);
	  while (index--) {
	    var data = matchData[index];
	    if ((noCustomizer && data[2])
	          ? data[1] !== object[data[0]]
	          : !(data[0] in object)
	        ) {
	      return false;
	    }
	  }
	  while (++index < length) {
	    data = matchData[index];
	    var key = data[0],
	        objValue = object[key],
	        srcValue = data[1];

	    if (noCustomizer && data[2]) {
	      if (objValue === undefined && !(key in object)) {
	        return false;
	      }
	    } else {
	      var result = customizer ? customizer(objValue, srcValue, key) : undefined;
	      if (!(result === undefined ? baseIsEqual(srcValue, objValue, customizer, true) : result)) {
	        return false;
	      }
	    }
	  }
	  return true;
	}

	module.exports = baseIsMatch;


/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	var baseIsEqualDeep = __webpack_require__(21),
	    isObject = __webpack_require__(30),
	    isObjectLike = __webpack_require__(31);

	/**
	 * The base implementation of `_.isEqual` without support for `this` binding
	 * `customizer` functions.
	 *
	 * @private
	 * @param {*} value The value to compare.
	 * @param {*} other The other value to compare.
	 * @param {Function} [customizer] The function to customize comparing values.
	 * @param {boolean} [isLoose] Specify performing partial comparisons.
	 * @param {Array} [stackA] Tracks traversed `value` objects.
	 * @param {Array} [stackB] Tracks traversed `other` objects.
	 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
	 */
	function baseIsEqual(value, other, customizer, isLoose, stackA, stackB) {
	  if (value === other) {
	    return true;
	  }
	  if (value == null || other == null || (!isObject(value) && !isObjectLike(other))) {
	    return value !== value && other !== other;
	  }
	  return baseIsEqualDeep(value, other, baseIsEqual, customizer, isLoose, stackA, stackB);
	}

	module.exports = baseIsEqual;


/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	var equalArrays = __webpack_require__(22),
	    equalByTag = __webpack_require__(24),
	    equalObjects = __webpack_require__(25),
	    isArray = __webpack_require__(38),
	    isTypedArray = __webpack_require__(41);

	/** `Object#toString` result references. */
	var argsTag = '[object Arguments]',
	    arrayTag = '[object Array]',
	    objectTag = '[object Object]';

	/** Used for native method references. */
	var objectProto = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty = objectProto.hasOwnProperty;

	/**
	 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
	 * of values.
	 */
	var objToString = objectProto.toString;

	/**
	 * A specialized version of `baseIsEqual` for arrays and objects which performs
	 * deep comparisons and tracks traversed objects enabling objects with circular
	 * references to be compared.
	 *
	 * @private
	 * @param {Object} object The object to compare.
	 * @param {Object} other The other object to compare.
	 * @param {Function} equalFunc The function to determine equivalents of values.
	 * @param {Function} [customizer] The function to customize comparing objects.
	 * @param {boolean} [isLoose] Specify performing partial comparisons.
	 * @param {Array} [stackA=[]] Tracks traversed `value` objects.
	 * @param {Array} [stackB=[]] Tracks traversed `other` objects.
	 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
	 */
	function baseIsEqualDeep(object, other, equalFunc, customizer, isLoose, stackA, stackB) {
	  var objIsArr = isArray(object),
	      othIsArr = isArray(other),
	      objTag = arrayTag,
	      othTag = arrayTag;

	  if (!objIsArr) {
	    objTag = objToString.call(object);
	    if (objTag == argsTag) {
	      objTag = objectTag;
	    } else if (objTag != objectTag) {
	      objIsArr = isTypedArray(object);
	    }
	  }
	  if (!othIsArr) {
	    othTag = objToString.call(other);
	    if (othTag == argsTag) {
	      othTag = objectTag;
	    } else if (othTag != objectTag) {
	      othIsArr = isTypedArray(other);
	    }
	  }
	  var objIsObj = objTag == objectTag,
	      othIsObj = othTag == objectTag,
	      isSameTag = objTag == othTag;

	  if (isSameTag && !(objIsArr || objIsObj)) {
	    return equalByTag(object, other, objTag);
	  }
	  if (!isLoose) {
	    var objIsWrapped = objIsObj && hasOwnProperty.call(object, '__wrapped__'),
	        othIsWrapped = othIsObj && hasOwnProperty.call(other, '__wrapped__');

	    if (objIsWrapped || othIsWrapped) {
	      return equalFunc(objIsWrapped ? object.value() : object, othIsWrapped ? other.value() : other, customizer, isLoose, stackA, stackB);
	    }
	  }
	  if (!isSameTag) {
	    return false;
	  }
	  // Assume cyclic values are equal.
	  // For more information on detecting circular references see https://es5.github.io/#JO.
	  stackA || (stackA = []);
	  stackB || (stackB = []);

	  var length = stackA.length;
	  while (length--) {
	    if (stackA[length] == object) {
	      return stackB[length] == other;
	    }
	  }
	  // Add `object` and `other` to the stack of traversed objects.
	  stackA.push(object);
	  stackB.push(other);

	  var result = (objIsArr ? equalArrays : equalObjects)(object, other, equalFunc, customizer, isLoose, stackA, stackB);

	  stackA.pop();
	  stackB.pop();

	  return result;
	}

	module.exports = baseIsEqualDeep;


/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	var arraySome = __webpack_require__(23);

	/**
	 * A specialized version of `baseIsEqualDeep` for arrays with support for
	 * partial deep comparisons.
	 *
	 * @private
	 * @param {Array} array The array to compare.
	 * @param {Array} other The other array to compare.
	 * @param {Function} equalFunc The function to determine equivalents of values.
	 * @param {Function} [customizer] The function to customize comparing arrays.
	 * @param {boolean} [isLoose] Specify performing partial comparisons.
	 * @param {Array} [stackA] Tracks traversed `value` objects.
	 * @param {Array} [stackB] Tracks traversed `other` objects.
	 * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
	 */
	function equalArrays(array, other, equalFunc, customizer, isLoose, stackA, stackB) {
	  var index = -1,
	      arrLength = array.length,
	      othLength = other.length;

	  if (arrLength != othLength && !(isLoose && othLength > arrLength)) {
	    return false;
	  }
	  // Ignore non-index properties.
	  while (++index < arrLength) {
	    var arrValue = array[index],
	        othValue = other[index],
	        result = customizer ? customizer(isLoose ? othValue : arrValue, isLoose ? arrValue : othValue, index) : undefined;

	    if (result !== undefined) {
	      if (result) {
	        continue;
	      }
	      return false;
	    }
	    // Recursively compare arrays (susceptible to call stack limits).
	    if (isLoose) {
	      if (!arraySome(other, function(othValue) {
	            return arrValue === othValue || equalFunc(arrValue, othValue, customizer, isLoose, stackA, stackB);
	          })) {
	        return false;
	      }
	    } else if (!(arrValue === othValue || equalFunc(arrValue, othValue, customizer, isLoose, stackA, stackB))) {
	      return false;
	    }
	  }
	  return true;
	}

	module.exports = equalArrays;


/***/ },
/* 23 */
/***/ function(module, exports) {

	/**
	 * A specialized version of `_.some` for arrays without support for callback
	 * shorthands and `this` binding.
	 *
	 * @private
	 * @param {Array} array The array to iterate over.
	 * @param {Function} predicate The function invoked per iteration.
	 * @returns {boolean} Returns `true` if any element passes the predicate check,
	 *  else `false`.
	 */
	function arraySome(array, predicate) {
	  var index = -1,
	      length = array.length;

	  while (++index < length) {
	    if (predicate(array[index], index, array)) {
	      return true;
	    }
	  }
	  return false;
	}

	module.exports = arraySome;


/***/ },
/* 24 */
/***/ function(module, exports) {

	/** `Object#toString` result references. */
	var boolTag = '[object Boolean]',
	    dateTag = '[object Date]',
	    errorTag = '[object Error]',
	    numberTag = '[object Number]',
	    regexpTag = '[object RegExp]',
	    stringTag = '[object String]';

	/**
	 * A specialized version of `baseIsEqualDeep` for comparing objects of
	 * the same `toStringTag`.
	 *
	 * **Note:** This function only supports comparing values with tags of
	 * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
	 *
	 * @private
	 * @param {Object} object The object to compare.
	 * @param {Object} other The other object to compare.
	 * @param {string} tag The `toStringTag` of the objects to compare.
	 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
	 */
	function equalByTag(object, other, tag) {
	  switch (tag) {
	    case boolTag:
	    case dateTag:
	      // Coerce dates and booleans to numbers, dates to milliseconds and booleans
	      // to `1` or `0` treating invalid dates coerced to `NaN` as not equal.
	      return +object == +other;

	    case errorTag:
	      return object.name == other.name && object.message == other.message;

	    case numberTag:
	      // Treat `NaN` vs. `NaN` as equal.
	      return (object != +object)
	        ? other != +other
	        : object == +other;

	    case regexpTag:
	    case stringTag:
	      // Coerce regexes to strings and treat strings primitives and string
	      // objects as equal. See https://es5.github.io/#x15.10.6.4 for more details.
	      return object == (other + '');
	  }
	  return false;
	}

	module.exports = equalByTag;


/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	var keys = __webpack_require__(26);

	/** Used for native method references. */
	var objectProto = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty = objectProto.hasOwnProperty;

	/**
	 * A specialized version of `baseIsEqualDeep` for objects with support for
	 * partial deep comparisons.
	 *
	 * @private
	 * @param {Object} object The object to compare.
	 * @param {Object} other The other object to compare.
	 * @param {Function} equalFunc The function to determine equivalents of values.
	 * @param {Function} [customizer] The function to customize comparing values.
	 * @param {boolean} [isLoose] Specify performing partial comparisons.
	 * @param {Array} [stackA] Tracks traversed `value` objects.
	 * @param {Array} [stackB] Tracks traversed `other` objects.
	 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
	 */
	function equalObjects(object, other, equalFunc, customizer, isLoose, stackA, stackB) {
	  var objProps = keys(object),
	      objLength = objProps.length,
	      othProps = keys(other),
	      othLength = othProps.length;

	  if (objLength != othLength && !isLoose) {
	    return false;
	  }
	  var index = objLength;
	  while (index--) {
	    var key = objProps[index];
	    if (!(isLoose ? key in other : hasOwnProperty.call(other, key))) {
	      return false;
	    }
	  }
	  var skipCtor = isLoose;
	  while (++index < objLength) {
	    key = objProps[index];
	    var objValue = object[key],
	        othValue = other[key],
	        result = customizer ? customizer(isLoose ? othValue : objValue, isLoose? objValue : othValue, key) : undefined;

	    // Recursively compare objects (susceptible to call stack limits).
	    if (!(result === undefined ? equalFunc(objValue, othValue, customizer, isLoose, stackA, stackB) : result)) {
	      return false;
	    }
	    skipCtor || (skipCtor = key == 'constructor');
	  }
	  if (!skipCtor) {
	    var objCtor = object.constructor,
	        othCtor = other.constructor;

	    // Non `Object` object instances with different constructors are not equal.
	    if (objCtor != othCtor &&
	        ('constructor' in object && 'constructor' in other) &&
	        !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
	          typeof othCtor == 'function' && othCtor instanceof othCtor)) {
	      return false;
	    }
	  }
	  return true;
	}

	module.exports = equalObjects;


/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	var getNative = __webpack_require__(27),
	    isArrayLike = __webpack_require__(32),
	    isObject = __webpack_require__(30),
	    shimKeys = __webpack_require__(36);

	/* Native method references for those with the same name as other `lodash` methods. */
	var nativeKeys = getNative(Object, 'keys');

	/**
	 * Creates an array of the own enumerable property names of `object`.
	 *
	 * **Note:** Non-object values are coerced to objects. See the
	 * [ES spec](http://ecma-international.org/ecma-262/6.0/#sec-object.keys)
	 * for more details.
	 *
	 * @static
	 * @memberOf _
	 * @category Object
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 * @example
	 *
	 * function Foo() {
	 *   this.a = 1;
	 *   this.b = 2;
	 * }
	 *
	 * Foo.prototype.c = 3;
	 *
	 * _.keys(new Foo);
	 * // => ['a', 'b'] (iteration order is not guaranteed)
	 *
	 * _.keys('hi');
	 * // => ['0', '1']
	 */
	var keys = !nativeKeys ? shimKeys : function(object) {
	  var Ctor = object == null ? undefined : object.constructor;
	  if ((typeof Ctor == 'function' && Ctor.prototype === object) ||
	      (typeof object != 'function' && isArrayLike(object))) {
	    return shimKeys(object);
	  }
	  return isObject(object) ? nativeKeys(object) : [];
	};

	module.exports = keys;


/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	var isNative = __webpack_require__(28);

	/**
	 * Gets the native function at `key` of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @param {string} key The key of the method to get.
	 * @returns {*} Returns the function if it's native, else `undefined`.
	 */
	function getNative(object, key) {
	  var value = object == null ? undefined : object[key];
	  return isNative(value) ? value : undefined;
	}

	module.exports = getNative;


/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	var isFunction = __webpack_require__(29),
	    isObjectLike = __webpack_require__(31);

	/** Used to detect host constructors (Safari > 5). */
	var reIsHostCtor = /^\[object .+?Constructor\]$/;

	/** Used for native method references. */
	var objectProto = Object.prototype;

	/** Used to resolve the decompiled source of functions. */
	var fnToString = Function.prototype.toString;

	/** Used to check objects for own properties. */
	var hasOwnProperty = objectProto.hasOwnProperty;

	/** Used to detect if a method is native. */
	var reIsNative = RegExp('^' +
	  fnToString.call(hasOwnProperty).replace(/[\\^$.*+?()[\]{}|]/g, '\\$&')
	  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
	);

	/**
	 * Checks if `value` is a native function.
	 *
	 * @static
	 * @memberOf _
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a native function, else `false`.
	 * @example
	 *
	 * _.isNative(Array.prototype.push);
	 * // => true
	 *
	 * _.isNative(_);
	 * // => false
	 */
	function isNative(value) {
	  if (value == null) {
	    return false;
	  }
	  if (isFunction(value)) {
	    return reIsNative.test(fnToString.call(value));
	  }
	  return isObjectLike(value) && reIsHostCtor.test(value);
	}

	module.exports = isNative;


/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	var isObject = __webpack_require__(30);

	/** `Object#toString` result references. */
	var funcTag = '[object Function]';

	/** Used for native method references. */
	var objectProto = Object.prototype;

	/**
	 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
	 * of values.
	 */
	var objToString = objectProto.toString;

	/**
	 * Checks if `value` is classified as a `Function` object.
	 *
	 * @static
	 * @memberOf _
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
	 * @example
	 *
	 * _.isFunction(_);
	 * // => true
	 *
	 * _.isFunction(/abc/);
	 * // => false
	 */
	function isFunction(value) {
	  // The use of `Object#toString` avoids issues with the `typeof` operator
	  // in older versions of Chrome and Safari which return 'function' for regexes
	  // and Safari 8 which returns 'object' for typed array constructors.
	  return isObject(value) && objToString.call(value) == funcTag;
	}

	module.exports = isFunction;


/***/ },
/* 30 */
/***/ function(module, exports) {

	/**
	 * Checks if `value` is the [language type](https://es5.github.io/#x8) of `Object`.
	 * (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
	 *
	 * @static
	 * @memberOf _
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
	 * @example
	 *
	 * _.isObject({});
	 * // => true
	 *
	 * _.isObject([1, 2, 3]);
	 * // => true
	 *
	 * _.isObject(1);
	 * // => false
	 */
	function isObject(value) {
	  // Avoid a V8 JIT bug in Chrome 19-20.
	  // See https://code.google.com/p/v8/issues/detail?id=2291 for more details.
	  var type = typeof value;
	  return !!value && (type == 'object' || type == 'function');
	}

	module.exports = isObject;


/***/ },
/* 31 */
/***/ function(module, exports) {

	/**
	 * Checks if `value` is object-like.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
	 */
	function isObjectLike(value) {
	  return !!value && typeof value == 'object';
	}

	module.exports = isObjectLike;


/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	var getLength = __webpack_require__(33),
	    isLength = __webpack_require__(35);

	/**
	 * Checks if `value` is array-like.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
	 */
	function isArrayLike(value) {
	  return value != null && isLength(getLength(value));
	}

	module.exports = isArrayLike;


/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

	var baseProperty = __webpack_require__(34);

	/**
	 * Gets the "length" property value of `object`.
	 *
	 * **Note:** This function is used to avoid a [JIT bug](https://bugs.webkit.org/show_bug.cgi?id=142792)
	 * that affects Safari on at least iOS 8.1-8.3 ARM64.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {*} Returns the "length" value.
	 */
	var getLength = baseProperty('length');

	module.exports = getLength;


/***/ },
/* 34 */
/***/ function(module, exports) {

	/**
	 * The base implementation of `_.property` without support for deep paths.
	 *
	 * @private
	 * @param {string} key The key of the property to get.
	 * @returns {Function} Returns the new function.
	 */
	function baseProperty(key) {
	  return function(object) {
	    return object == null ? undefined : object[key];
	  };
	}

	module.exports = baseProperty;


/***/ },
/* 35 */
/***/ function(module, exports) {

	/**
	 * Used as the [maximum length](http://ecma-international.org/ecma-262/6.0/#sec-number.max_safe_integer)
	 * of an array-like value.
	 */
	var MAX_SAFE_INTEGER = 9007199254740991;

	/**
	 * Checks if `value` is a valid array-like length.
	 *
	 * **Note:** This function is based on [`ToLength`](http://ecma-international.org/ecma-262/6.0/#sec-tolength).
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
	 */
	function isLength(value) {
	  return typeof value == 'number' && value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
	}

	module.exports = isLength;


/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

	var isArguments = __webpack_require__(37),
	    isArray = __webpack_require__(38),
	    isIndex = __webpack_require__(39),
	    isLength = __webpack_require__(35),
	    keysIn = __webpack_require__(40);

	/** Used for native method references. */
	var objectProto = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty = objectProto.hasOwnProperty;

	/**
	 * A fallback implementation of `Object.keys` which creates an array of the
	 * own enumerable property names of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 */
	function shimKeys(object) {
	  var props = keysIn(object),
	      propsLength = props.length,
	      length = propsLength && object.length;

	  var allowIndexes = !!length && isLength(length) &&
	    (isArray(object) || isArguments(object));

	  var index = -1,
	      result = [];

	  while (++index < propsLength) {
	    var key = props[index];
	    if ((allowIndexes && isIndex(key, length)) || hasOwnProperty.call(object, key)) {
	      result.push(key);
	    }
	  }
	  return result;
	}

	module.exports = shimKeys;


/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	var isArrayLike = __webpack_require__(32),
	    isObjectLike = __webpack_require__(31);

	/** Used for native method references. */
	var objectProto = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty = objectProto.hasOwnProperty;

	/** Native method references. */
	var propertyIsEnumerable = objectProto.propertyIsEnumerable;

	/**
	 * Checks if `value` is classified as an `arguments` object.
	 *
	 * @static
	 * @memberOf _
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
	 * @example
	 *
	 * _.isArguments(function() { return arguments; }());
	 * // => true
	 *
	 * _.isArguments([1, 2, 3]);
	 * // => false
	 */
	function isArguments(value) {
	  return isObjectLike(value) && isArrayLike(value) &&
	    hasOwnProperty.call(value, 'callee') && !propertyIsEnumerable.call(value, 'callee');
	}

	module.exports = isArguments;


/***/ },
/* 38 */
/***/ function(module, exports, __webpack_require__) {

	var getNative = __webpack_require__(27),
	    isLength = __webpack_require__(35),
	    isObjectLike = __webpack_require__(31);

	/** `Object#toString` result references. */
	var arrayTag = '[object Array]';

	/** Used for native method references. */
	var objectProto = Object.prototype;

	/**
	 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
	 * of values.
	 */
	var objToString = objectProto.toString;

	/* Native method references for those with the same name as other `lodash` methods. */
	var nativeIsArray = getNative(Array, 'isArray');

	/**
	 * Checks if `value` is classified as an `Array` object.
	 *
	 * @static
	 * @memberOf _
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
	 * @example
	 *
	 * _.isArray([1, 2, 3]);
	 * // => true
	 *
	 * _.isArray(function() { return arguments; }());
	 * // => false
	 */
	var isArray = nativeIsArray || function(value) {
	  return isObjectLike(value) && isLength(value.length) && objToString.call(value) == arrayTag;
	};

	module.exports = isArray;


/***/ },
/* 39 */
/***/ function(module, exports) {

	/** Used to detect unsigned integer values. */
	var reIsUint = /^\d+$/;

	/**
	 * Used as the [maximum length](http://ecma-international.org/ecma-262/6.0/#sec-number.max_safe_integer)
	 * of an array-like value.
	 */
	var MAX_SAFE_INTEGER = 9007199254740991;

	/**
	 * Checks if `value` is a valid array-like index.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
	 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
	 */
	function isIndex(value, length) {
	  value = (typeof value == 'number' || reIsUint.test(value)) ? +value : -1;
	  length = length == null ? MAX_SAFE_INTEGER : length;
	  return value > -1 && value % 1 == 0 && value < length;
	}

	module.exports = isIndex;


/***/ },
/* 40 */
/***/ function(module, exports, __webpack_require__) {

	var isArguments = __webpack_require__(37),
	    isArray = __webpack_require__(38),
	    isIndex = __webpack_require__(39),
	    isLength = __webpack_require__(35),
	    isObject = __webpack_require__(30);

	/** Used for native method references. */
	var objectProto = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty = objectProto.hasOwnProperty;

	/**
	 * Creates an array of the own and inherited enumerable property names of `object`.
	 *
	 * **Note:** Non-object values are coerced to objects.
	 *
	 * @static
	 * @memberOf _
	 * @category Object
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 * @example
	 *
	 * function Foo() {
	 *   this.a = 1;
	 *   this.b = 2;
	 * }
	 *
	 * Foo.prototype.c = 3;
	 *
	 * _.keysIn(new Foo);
	 * // => ['a', 'b', 'c'] (iteration order is not guaranteed)
	 */
	function keysIn(object) {
	  if (object == null) {
	    return [];
	  }
	  if (!isObject(object)) {
	    object = Object(object);
	  }
	  var length = object.length;
	  length = (length && isLength(length) &&
	    (isArray(object) || isArguments(object)) && length) || 0;

	  var Ctor = object.constructor,
	      index = -1,
	      isProto = typeof Ctor == 'function' && Ctor.prototype === object,
	      result = Array(length),
	      skipIndexes = length > 0;

	  while (++index < length) {
	    result[index] = (index + '');
	  }
	  for (var key in object) {
	    if (!(skipIndexes && isIndex(key, length)) &&
	        !(key == 'constructor' && (isProto || !hasOwnProperty.call(object, key)))) {
	      result.push(key);
	    }
	  }
	  return result;
	}

	module.exports = keysIn;


/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

	var isLength = __webpack_require__(35),
	    isObjectLike = __webpack_require__(31);

	/** `Object#toString` result references. */
	var argsTag = '[object Arguments]',
	    arrayTag = '[object Array]',
	    boolTag = '[object Boolean]',
	    dateTag = '[object Date]',
	    errorTag = '[object Error]',
	    funcTag = '[object Function]',
	    mapTag = '[object Map]',
	    numberTag = '[object Number]',
	    objectTag = '[object Object]',
	    regexpTag = '[object RegExp]',
	    setTag = '[object Set]',
	    stringTag = '[object String]',
	    weakMapTag = '[object WeakMap]';

	var arrayBufferTag = '[object ArrayBuffer]',
	    float32Tag = '[object Float32Array]',
	    float64Tag = '[object Float64Array]',
	    int8Tag = '[object Int8Array]',
	    int16Tag = '[object Int16Array]',
	    int32Tag = '[object Int32Array]',
	    uint8Tag = '[object Uint8Array]',
	    uint8ClampedTag = '[object Uint8ClampedArray]',
	    uint16Tag = '[object Uint16Array]',
	    uint32Tag = '[object Uint32Array]';

	/** Used to identify `toStringTag` values of typed arrays. */
	var typedArrayTags = {};
	typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
	typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
	typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
	typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
	typedArrayTags[uint32Tag] = true;
	typedArrayTags[argsTag] = typedArrayTags[arrayTag] =
	typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
	typedArrayTags[dateTag] = typedArrayTags[errorTag] =
	typedArrayTags[funcTag] = typedArrayTags[mapTag] =
	typedArrayTags[numberTag] = typedArrayTags[objectTag] =
	typedArrayTags[regexpTag] = typedArrayTags[setTag] =
	typedArrayTags[stringTag] = typedArrayTags[weakMapTag] = false;

	/** Used for native method references. */
	var objectProto = Object.prototype;

	/**
	 * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
	 * of values.
	 */
	var objToString = objectProto.toString;

	/**
	 * Checks if `value` is classified as a typed array.
	 *
	 * @static
	 * @memberOf _
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
	 * @example
	 *
	 * _.isTypedArray(new Uint8Array);
	 * // => true
	 *
	 * _.isTypedArray([]);
	 * // => false
	 */
	function isTypedArray(value) {
	  return isObjectLike(value) && isLength(value.length) && !!typedArrayTags[objToString.call(value)];
	}

	module.exports = isTypedArray;


/***/ },
/* 42 */
/***/ function(module, exports, __webpack_require__) {

	var isObject = __webpack_require__(30);

	/**
	 * Converts `value` to an object if it's not one.
	 *
	 * @private
	 * @param {*} value The value to process.
	 * @returns {Object} Returns the object.
	 */
	function toObject(value) {
	  return isObject(value) ? value : Object(value);
	}

	module.exports = toObject;


/***/ },
/* 43 */
/***/ function(module, exports, __webpack_require__) {

	var isStrictComparable = __webpack_require__(44),
	    pairs = __webpack_require__(45);

	/**
	 * Gets the propery names, values, and compare flags of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the match data of `object`.
	 */
	function getMatchData(object) {
	  var result = pairs(object),
	      length = result.length;

	  while (length--) {
	    result[length][2] = isStrictComparable(result[length][1]);
	  }
	  return result;
	}

	module.exports = getMatchData;


/***/ },
/* 44 */
/***/ function(module, exports, __webpack_require__) {

	var isObject = __webpack_require__(30);

	/**
	 * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` if suitable for strict
	 *  equality comparisons, else `false`.
	 */
	function isStrictComparable(value) {
	  return value === value && !isObject(value);
	}

	module.exports = isStrictComparable;


/***/ },
/* 45 */
/***/ function(module, exports, __webpack_require__) {

	var keys = __webpack_require__(26),
	    toObject = __webpack_require__(42);

	/**
	 * Creates a two dimensional array of the key-value pairs for `object`,
	 * e.g. `[[key1, value1], [key2, value2]]`.
	 *
	 * @static
	 * @memberOf _
	 * @category Object
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the new array of key-value pairs.
	 * @example
	 *
	 * _.pairs({ 'barney': 36, 'fred': 40 });
	 * // => [['barney', 36], ['fred', 40]] (iteration order is not guaranteed)
	 */
	function pairs(object) {
	  object = toObject(object);

	  var index = -1,
	      props = keys(object),
	      length = props.length,
	      result = Array(length);

	  while (++index < length) {
	    var key = props[index];
	    result[index] = [key, object[key]];
	  }
	  return result;
	}

	module.exports = pairs;


/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

	var baseGet = __webpack_require__(47),
	    baseIsEqual = __webpack_require__(20),
	    baseSlice = __webpack_require__(48),
	    isArray = __webpack_require__(38),
	    isKey = __webpack_require__(49),
	    isStrictComparable = __webpack_require__(44),
	    last = __webpack_require__(50),
	    toObject = __webpack_require__(42),
	    toPath = __webpack_require__(51);

	/**
	 * The base implementation of `_.matchesProperty` which does not clone `srcValue`.
	 *
	 * @private
	 * @param {string} path The path of the property to get.
	 * @param {*} srcValue The value to compare.
	 * @returns {Function} Returns the new function.
	 */
	function baseMatchesProperty(path, srcValue) {
	  var isArr = isArray(path),
	      isCommon = isKey(path) && isStrictComparable(srcValue),
	      pathKey = (path + '');

	  path = toPath(path);
	  return function(object) {
	    if (object == null) {
	      return false;
	    }
	    var key = pathKey;
	    object = toObject(object);
	    if ((isArr || !isCommon) && !(key in object)) {
	      object = path.length == 1 ? object : baseGet(object, baseSlice(path, 0, -1));
	      if (object == null) {
	        return false;
	      }
	      key = last(path);
	      object = toObject(object);
	    }
	    return object[key] === srcValue
	      ? (srcValue !== undefined || (key in object))
	      : baseIsEqual(srcValue, object[key], undefined, true);
	  };
	}

	module.exports = baseMatchesProperty;


/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

	var toObject = __webpack_require__(42);

	/**
	 * The base implementation of `get` without support for string paths
	 * and default values.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @param {Array} path The path of the property to get.
	 * @param {string} [pathKey] The key representation of path.
	 * @returns {*} Returns the resolved value.
	 */
	function baseGet(object, path, pathKey) {
	  if (object == null) {
	    return;
	  }
	  if (pathKey !== undefined && pathKey in toObject(object)) {
	    path = [pathKey];
	  }
	  var index = 0,
	      length = path.length;

	  while (object != null && index < length) {
	    object = object[path[index++]];
	  }
	  return (index && index == length) ? object : undefined;
	}

	module.exports = baseGet;


/***/ },
/* 48 */
/***/ function(module, exports) {

	/**
	 * The base implementation of `_.slice` without an iteratee call guard.
	 *
	 * @private
	 * @param {Array} array The array to slice.
	 * @param {number} [start=0] The start position.
	 * @param {number} [end=array.length] The end position.
	 * @returns {Array} Returns the slice of `array`.
	 */
	function baseSlice(array, start, end) {
	  var index = -1,
	      length = array.length;

	  start = start == null ? 0 : (+start || 0);
	  if (start < 0) {
	    start = -start > length ? 0 : (length + start);
	  }
	  end = (end === undefined || end > length) ? length : (+end || 0);
	  if (end < 0) {
	    end += length;
	  }
	  length = start > end ? 0 : ((end - start) >>> 0);
	  start >>>= 0;

	  var result = Array(length);
	  while (++index < length) {
	    result[index] = array[index + start];
	  }
	  return result;
	}

	module.exports = baseSlice;


/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

	var isArray = __webpack_require__(38),
	    toObject = __webpack_require__(42);

	/** Used to match property names within property paths. */
	var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\n\\]|\\.)*?\1)\]/,
	    reIsPlainProp = /^\w*$/;

	/**
	 * Checks if `value` is a property name and not a property path.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @param {Object} [object] The object to query keys on.
	 * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
	 */
	function isKey(value, object) {
	  var type = typeof value;
	  if ((type == 'string' && reIsPlainProp.test(value)) || type == 'number') {
	    return true;
	  }
	  if (isArray(value)) {
	    return false;
	  }
	  var result = !reIsDeepProp.test(value);
	  return result || (object != null && value in toObject(object));
	}

	module.exports = isKey;


/***/ },
/* 50 */
/***/ function(module, exports) {

	/**
	 * Gets the last element of `array`.
	 *
	 * @static
	 * @memberOf _
	 * @category Array
	 * @param {Array} array The array to query.
	 * @returns {*} Returns the last element of `array`.
	 * @example
	 *
	 * _.last([1, 2, 3]);
	 * // => 3
	 */
	function last(array) {
	  var length = array ? array.length : 0;
	  return length ? array[length - 1] : undefined;
	}

	module.exports = last;


/***/ },
/* 51 */
/***/ function(module, exports, __webpack_require__) {

	var baseToString = __webpack_require__(52),
	    isArray = __webpack_require__(38);

	/** Used to match property names within property paths. */
	var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\n\\]|\\.)*?)\2)\]/g;

	/** Used to match backslashes in property paths. */
	var reEscapeChar = /\\(\\)?/g;

	/**
	 * Converts `value` to property path array if it's not one.
	 *
	 * @private
	 * @param {*} value The value to process.
	 * @returns {Array} Returns the property path array.
	 */
	function toPath(value) {
	  if (isArray(value)) {
	    return value;
	  }
	  var result = [];
	  baseToString(value).replace(rePropName, function(match, number, quote, string) {
	    result.push(quote ? string.replace(reEscapeChar, '$1') : (number || match));
	  });
	  return result;
	}

	module.exports = toPath;


/***/ },
/* 52 */
/***/ function(module, exports) {

	/**
	 * Converts `value` to a string if it's not one. An empty string is returned
	 * for `null` or `undefined` values.
	 *
	 * @private
	 * @param {*} value The value to process.
	 * @returns {string} Returns the string.
	 */
	function baseToString(value) {
	  return value == null ? '' : (value + '');
	}

	module.exports = baseToString;


/***/ },
/* 53 */
/***/ function(module, exports, __webpack_require__) {

	var identity = __webpack_require__(54);

	/**
	 * A specialized version of `baseCallback` which only supports `this` binding
	 * and specifying the number of arguments to provide to `func`.
	 *
	 * @private
	 * @param {Function} func The function to bind.
	 * @param {*} thisArg The `this` binding of `func`.
	 * @param {number} [argCount] The number of arguments to provide to `func`.
	 * @returns {Function} Returns the callback.
	 */
	function bindCallback(func, thisArg, argCount) {
	  if (typeof func != 'function') {
	    return identity;
	  }
	  if (thisArg === undefined) {
	    return func;
	  }
	  switch (argCount) {
	    case 1: return function(value) {
	      return func.call(thisArg, value);
	    };
	    case 3: return function(value, index, collection) {
	      return func.call(thisArg, value, index, collection);
	    };
	    case 4: return function(accumulator, value, index, collection) {
	      return func.call(thisArg, accumulator, value, index, collection);
	    };
	    case 5: return function(value, other, key, object, source) {
	      return func.call(thisArg, value, other, key, object, source);
	    };
	  }
	  return function() {
	    return func.apply(thisArg, arguments);
	  };
	}

	module.exports = bindCallback;


/***/ },
/* 54 */
/***/ function(module, exports) {

	/**
	 * This method returns the first argument provided to it.
	 *
	 * @static
	 * @memberOf _
	 * @category Utility
	 * @param {*} value Any value.
	 * @returns {*} Returns `value`.
	 * @example
	 *
	 * var object = { 'user': 'fred' };
	 *
	 * _.identity(object) === object;
	 * // => true
	 */
	function identity(value) {
	  return value;
	}

	module.exports = identity;


/***/ },
/* 55 */
/***/ function(module, exports, __webpack_require__) {

	var baseProperty = __webpack_require__(34),
	    basePropertyDeep = __webpack_require__(56),
	    isKey = __webpack_require__(49);

	/**
	 * Creates a function that returns the property value at `path` on a
	 * given object.
	 *
	 * @static
	 * @memberOf _
	 * @category Utility
	 * @param {Array|string} path The path of the property to get.
	 * @returns {Function} Returns the new function.
	 * @example
	 *
	 * var objects = [
	 *   { 'a': { 'b': { 'c': 2 } } },
	 *   { 'a': { 'b': { 'c': 1 } } }
	 * ];
	 *
	 * _.map(objects, _.property('a.b.c'));
	 * // => [2, 1]
	 *
	 * _.pluck(_.sortBy(objects, _.property(['a', 'b', 'c'])), 'a.b.c');
	 * // => [1, 2]
	 */
	function property(path) {
	  return isKey(path) ? baseProperty(path) : basePropertyDeep(path);
	}

	module.exports = property;


/***/ },
/* 56 */
/***/ function(module, exports, __webpack_require__) {

	var baseGet = __webpack_require__(47),
	    toPath = __webpack_require__(51);

	/**
	 * A specialized version of `baseProperty` which supports deep paths.
	 *
	 * @private
	 * @param {Array|string} path The path of the property to get.
	 * @returns {Function} Returns the new function.
	 */
	function basePropertyDeep(path) {
	  var pathKey = (path + '');
	  path = toPath(path);
	  return function(object) {
	    return baseGet(object, path, pathKey);
	  };
	}

	module.exports = basePropertyDeep;


/***/ },
/* 57 */
/***/ function(module, exports, __webpack_require__) {

	var baseIndexOf = __webpack_require__(58),
	    cacheIndexOf = __webpack_require__(60),
	    createCache = __webpack_require__(61);

	/** Used as the size to enable large array optimizations. */
	var LARGE_ARRAY_SIZE = 200;

	/**
	 * The base implementation of `_.uniq` without support for callback shorthands
	 * and `this` binding.
	 *
	 * @private
	 * @param {Array} array The array to inspect.
	 * @param {Function} [iteratee] The function invoked per iteration.
	 * @returns {Array} Returns the new duplicate free array.
	 */
	function baseUniq(array, iteratee) {
	  var index = -1,
	      indexOf = baseIndexOf,
	      length = array.length,
	      isCommon = true,
	      isLarge = isCommon && length >= LARGE_ARRAY_SIZE,
	      seen = isLarge ? createCache() : null,
	      result = [];

	  if (seen) {
	    indexOf = cacheIndexOf;
	    isCommon = false;
	  } else {
	    isLarge = false;
	    seen = iteratee ? [] : result;
	  }
	  outer:
	  while (++index < length) {
	    var value = array[index],
	        computed = iteratee ? iteratee(value, index, array) : value;

	    if (isCommon && value === value) {
	      var seenIndex = seen.length;
	      while (seenIndex--) {
	        if (seen[seenIndex] === computed) {
	          continue outer;
	        }
	      }
	      if (iteratee) {
	        seen.push(computed);
	      }
	      result.push(value);
	    }
	    else if (indexOf(seen, computed, 0) < 0) {
	      if (iteratee || isLarge) {
	        seen.push(computed);
	      }
	      result.push(value);
	    }
	  }
	  return result;
	}

	module.exports = baseUniq;


/***/ },
/* 58 */
/***/ function(module, exports, __webpack_require__) {

	var indexOfNaN = __webpack_require__(59);

	/**
	 * The base implementation of `_.indexOf` without support for binary searches.
	 *
	 * @private
	 * @param {Array} array The array to search.
	 * @param {*} value The value to search for.
	 * @param {number} fromIndex The index to search from.
	 * @returns {number} Returns the index of the matched value, else `-1`.
	 */
	function baseIndexOf(array, value, fromIndex) {
	  if (value !== value) {
	    return indexOfNaN(array, fromIndex);
	  }
	  var index = fromIndex - 1,
	      length = array.length;

	  while (++index < length) {
	    if (array[index] === value) {
	      return index;
	    }
	  }
	  return -1;
	}

	module.exports = baseIndexOf;


/***/ },
/* 59 */
/***/ function(module, exports) {

	/**
	 * Gets the index at which the first occurrence of `NaN` is found in `array`.
	 *
	 * @private
	 * @param {Array} array The array to search.
	 * @param {number} fromIndex The index to search from.
	 * @param {boolean} [fromRight] Specify iterating from right to left.
	 * @returns {number} Returns the index of the matched `NaN`, else `-1`.
	 */
	function indexOfNaN(array, fromIndex, fromRight) {
	  var length = array.length,
	      index = fromIndex + (fromRight ? 0 : -1);

	  while ((fromRight ? index-- : ++index < length)) {
	    var other = array[index];
	    if (other !== other) {
	      return index;
	    }
	  }
	  return -1;
	}

	module.exports = indexOfNaN;


/***/ },
/* 60 */
/***/ function(module, exports, __webpack_require__) {

	var isObject = __webpack_require__(30);

	/**
	 * Checks if `value` is in `cache` mimicking the return signature of
	 * `_.indexOf` by returning `0` if the value is found, else `-1`.
	 *
	 * @private
	 * @param {Object} cache The cache to search.
	 * @param {*} value The value to search for.
	 * @returns {number} Returns `0` if `value` is found, else `-1`.
	 */
	function cacheIndexOf(cache, value) {
	  var data = cache.data,
	      result = (typeof value == 'string' || isObject(value)) ? data.set.has(value) : data.hash[value];

	  return result ? 0 : -1;
	}

	module.exports = cacheIndexOf;


/***/ },
/* 61 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {var SetCache = __webpack_require__(62),
	    getNative = __webpack_require__(27);

	/** Native method references. */
	var Set = getNative(global, 'Set');

	/* Native method references for those with the same name as other `lodash` methods. */
	var nativeCreate = getNative(Object, 'create');

	/**
	 * Creates a `Set` cache object to optimize linear searches of large arrays.
	 *
	 * @private
	 * @param {Array} [values] The values to cache.
	 * @returns {null|Object} Returns the new cache object if `Set` is supported, else `null`.
	 */
	function createCache(values) {
	  return (nativeCreate && Set) ? new SetCache(values) : null;
	}

	module.exports = createCache;

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 62 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {var cachePush = __webpack_require__(63),
	    getNative = __webpack_require__(27);

	/** Native method references. */
	var Set = getNative(global, 'Set');

	/* Native method references for those with the same name as other `lodash` methods. */
	var nativeCreate = getNative(Object, 'create');

	/**
	 *
	 * Creates a cache object to store unique values.
	 *
	 * @private
	 * @param {Array} [values] The values to cache.
	 */
	function SetCache(values) {
	  var length = values ? values.length : 0;

	  this.data = { 'hash': nativeCreate(null), 'set': new Set };
	  while (length--) {
	    this.push(values[length]);
	  }
	}

	// Add functions to the `Set` cache.
	SetCache.prototype.push = cachePush;

	module.exports = SetCache;

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 63 */
/***/ function(module, exports, __webpack_require__) {

	var isObject = __webpack_require__(30);

	/**
	 * Adds `value` to the cache.
	 *
	 * @private
	 * @name push
	 * @memberOf SetCache
	 * @param {*} value The value to cache.
	 */
	function cachePush(value) {
	  var data = this.data;
	  if (typeof value == 'string' || isObject(value)) {
	    data.set.add(value);
	  } else {
	    data.hash[value] = true;
	  }
	}

	module.exports = cachePush;


/***/ },
/* 64 */
/***/ function(module, exports, __webpack_require__) {

	var isArrayLike = __webpack_require__(32),
	    isIndex = __webpack_require__(39),
	    isObject = __webpack_require__(30);

	/**
	 * Checks if the provided arguments are from an iteratee call.
	 *
	 * @private
	 * @param {*} value The potential iteratee value argument.
	 * @param {*} index The potential iteratee index or key argument.
	 * @param {*} object The potential iteratee object argument.
	 * @returns {boolean} Returns `true` if the arguments are from an iteratee call, else `false`.
	 */
	function isIterateeCall(value, index, object) {
	  if (!isObject(object)) {
	    return false;
	  }
	  var type = typeof index;
	  if (type == 'number'
	      ? (isArrayLike(object) && isIndex(index, object.length))
	      : (type == 'string' && index in object)) {
	    var other = object[index];
	    return value === value ? (value === other) : (other !== other);
	  }
	  return false;
	}

	module.exports = isIterateeCall;


/***/ },
/* 65 */
/***/ function(module, exports) {

	/**
	 * An implementation of `_.uniq` optimized for sorted arrays without support
	 * for callback shorthands and `this` binding.
	 *
	 * @private
	 * @param {Array} array The array to inspect.
	 * @param {Function} [iteratee] The function invoked per iteration.
	 * @returns {Array} Returns the new duplicate free array.
	 */
	function sortedUniq(array, iteratee) {
	  var seen,
	      index = -1,
	      length = array.length,
	      resIndex = -1,
	      result = [];

	  while (++index < length) {
	    var value = array[index],
	        computed = iteratee ? iteratee(value, index, array) : value;

	    if (!index || seen !== computed) {
	      seen = computed;
	      result[++resIndex] = value;
	    }
	  }
	  return result;
	}

	module.exports = sortedUniq;


/***/ },
/* 66 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var ObjectSpecParser = __webpack_require__(67);

	module.exports = {
	    ObjectSpecParser: ObjectSpecParser
	};

/***/ },
/* 67 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; })();

	var _require = __webpack_require__(68);

	var getNameNamespaceFromEntityKey = _require.getNameNamespaceFromEntityKey;

	function parseRequireString(es) {
	  return es.trim().split(/\s+/);
	}

	function load(objectSpec, manager) {

	  function getEntityIdsFromRequireString(requireString) {
	    return parseRequireString(requireString).map(getNameNamespaceFromEntityKey).map(function (_ref) {
	      var _ref2 = _slicedToArray(_ref, 2);

	      var name = _ref2[0];
	      var namespace = _ref2[1];

	      return manager.getEntityByNameNamespace(name, namespace) || manager.addEntity({ name: name, namespace: namespace });
	    }).map(function (e) {
	      return e.id;
	    });
	  }

	  var factories = {},
	      reactions = {},
	      oscillators = {};

	  for (var entityKey in objectSpec) {
	    var entitySpec = objectSpec[entityKey];

	    var _getNameNamespaceFromEntityKey = getNameNamespaceFromEntityKey(entityKey);

	    var _getNameNamespaceFromEntityKey2 = _slicedToArray(_getNameNamespaceFromEntityKey, 2);

	    var _name = _getNameNamespaceFromEntityKey2[0];
	    var namespace = _getNameNamespaceFromEntityKey2[1];
	    var data = { name: _name, namespace: namespace };

	    if (entitySpec.value != null) {
	      data.initialValue = entitySpec.value;
	    }

	    var entity = manager.addEntity(data);

	    if (entitySpec.init) {
	      factories[entity.id] = entitySpec;
	    }

	    if (entitySpec.reactions) {
	      reactions[entity.id] = entitySpec;
	    }

	    if (entitySpec.oscillate) {
	      oscillators[entity.id] = entitySpec;
	    }
	  }

	  for (var id in factories) {
	    var entitySpec = factories[id];

	    if (entitySpec.init.require) {

	      var depIds = getEntityIdsFromRequireString(entitySpec.init.require);

	      manager.addFactory({
	        receiver: id,
	        procedure: entitySpec.init["do"],
	        dependencies: depIds
	      });
	    } else {

	      manager.addFactory({
	        receiver: id,
	        procedure: entitySpec.init
	      });
	    }
	  }

	  for (var id in oscillators) {
	    var entitySpec = oscillators[id],
	        targetId = getEntityIdsFromRequireString(entitySpec.target);

	    if (entitySpec.oscillate.require) {
	      var depIds = getEntityIdsFromRequireString(entitySpec.oscillate.require);

	      manager.addOscillator({
	        receiver: id,
	        target: targetId[0],
	        procedure: entitySpec.oscillate["do"],
	        dependencies: depIds
	      });
	    } else {

	      manager.addOscillator({
	        receiver: id,
	        target: targetId[0],
	        procedure: entitySpec.oscillate
	      });
	    }
	  }

	  for (var id in reactions) {
	    var entitySpec = reactions[id];

	    for (var triggerString in entitySpec.reactions) {
	      var reactionSpec = entitySpec.reactions[triggerString],
	          triggers = getEntityIdsFromRequireString(triggerString),
	          receiver = id;

	      if (reactionSpec.require) {
	        var supplements = getEntityIdsFromRequireString(reactionSpec.require);

	        manager.addReaction({
	          receiver: receiver,
	          triggers: triggers,
	          supplements: supplements,
	          procedure: reactionSpec["do"]
	        });
	      } else {

	        manager.addReaction({
	          receiver: receiver,
	          triggers: triggers,
	          procedure: reactionSpec
	        });
	      }
	    }
	  }
	}

	module.exports = {
	  load: load
	};

/***/ },
/* 68 */
/***/ function(module, exports) {

	'use strict';

	var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

	var newUid = (function () {
	  var id = 1;
	  return function () {
	    return id++;
	  };
	})();

	function getEntityKeyFromNameNamespace(name, namespace) {
	  if (!namespace) return name;
	  return [namespace, name].join('/');
	}

	function getNameNamespaceFromEntityKey(id) {
	  var _id$split = id.split('/');

	  var _id$split2 = _slicedToArray(_id$split, 2);

	  var namespace = _id$split2[0];
	  var name = _id$split2[1];

	  if (!name) {
	    name = namespace;
	    namespace = '';
	  }
	  return [name, namespace];
	}

	// ===== module interface =====

	module.exports = {
	  newUid: newUid,
	  getNameNamespaceFromEntityKey: getNameNamespaceFromEntityKey,
	  getEntityKeyFromNameNamespace: getEntityKeyFromNameNamespace
	};

/***/ },
/* 69 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var oscillators = __webpack_require__(70);

	module.exports = {
	  oscillators: oscillators
	};

/***/ },
/* 70 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var directionKeys = __webpack_require__(71),
	    mouseDrag = __webpack_require__(72),
	    mousePress = __webpack_require__(73),
	    requestAnimation = __webpack_require__(74);

	module.exports = {
	  directionKeys: directionKeys,
	  mouseDrag: mouseDrag,
	  mousePress: mousePress,
	  requestAnimation: requestAnimation
	};

/***/ },
/* 71 */
/***/ function(module, exports) {

	"use strict";

	module.exports = function createDirectionkeys(opts) {
	  var _ref = opts || {};

	  var autoFlush = _ref.autoFlush;

	  return function (sink) {

	    var state = {
	      forth: false,
	      back: false,
	      left: false,
	      right: false,
	      up: false,
	      down: false
	    };

	    sink(state);

	    function onKeyDown(e) {
	      var update = false;

	      switch (e.keyCode) {
	        case 38:
	        case 87:
	          if (!state.forth) {
	            state.forth = true;
	            update = true;
	          }
	          break;

	        case 37:
	        case 65:
	          if (!state.left) {
	            state.left = true;
	            update = true;
	          }
	          break;

	        case 40:
	        case 83:
	          if (!state.back) {
	            state.back = true;
	            update = true;
	          }
	          break;

	        case 39:
	        case 68:
	          if (!state.right) {
	            state.right = true;
	            update = true;
	          }
	          break;

	        case 82:
	          if (!state.up) {
	            state.up = true;
	            update = true;
	          }
	          break;

	        case 70:
	          if (!state.down) {
	            state.down = true;
	            update = true;
	          }
	      }

	      if (update) {
	        sink(state, autoFlush);
	      }
	    }

	    function onKeyUp(e) {
	      var update = false;

	      switch (e.keyCode) {
	        case 38:
	        case 87:
	          state.forth = false;
	          update = true;
	          break;

	        case 37:
	        case 65:
	          state.left = false;
	          update = true;
	          break;

	        case 40:
	        case 83:
	          state.back = false;
	          update = true;
	          break;

	        case 39:
	        case 68:
	          state.right = false;
	          update = true;
	          break;

	        case 82:
	          state.up = false;
	          update = true;
	          break;

	        case 70:
	          state.down = false;
	          update = true;
	      }

	      if (update) {
	        sink(state, autoFlush);
	      }
	    }

	    document.addEventListener("keyup", onKeyUp, false);
	    document.addEventListener("keydown", onKeyDown, false);
	  };
	};

/***/ },
/* 72 */
/***/ function(module, exports) {

	"use strict";

	module.exports = function createMouseDrag(opts) {
	  var _ref = opts || {};

	  var autoFlush = _ref.autoFlush;
	  var _ref$element = _ref.element;
	  var element = _ref$element === undefined ? document : _ref$element;

	  return function (sink) {

	    var x = 0,
	        y = 0,
	        dragging = false,
	        oldDeltaX = 0,
	        oldDeltaY = 0;

	    var delta = { x: 0, y: 0 };

	    sink(delta);

	    function onMouseMove(e) {
	      if (dragging) {
	        oldDeltaX = delta.x;
	        oldDeltaY = delta.y;
	        delta.x = x - e.screenX;
	        delta.y = y - e.screenY;
	        if (delta.x !== oldDeltaX || delta.y !== oldDeltaY) {
	          sink(delta, autoFlush);
	        }
	        x = e.screenX;
	        y = e.screenY;
	      }
	    }

	    function onMouseUp() {
	      delta.x = oldDeltaX = 0;
	      delta.y = oldDeltaY = 0;
	      sink(delta, autoFlush);
	      dragging = false;
	    }

	    function onMouseDown(e) {
	      if (e.button === 0) {
	        x = e.screenX;
	        y = e.screenY;
	        dragging = true;
	      }
	    }

	    element.addEventListener("mousedown", onMouseDown);
	    document.addEventListener("mousemove", onMouseMove);
	    document.addEventListener("mouseup", onMouseUp);
	  };
	};

/***/ },
/* 73 */
/***/ function(module, exports) {

	"use strict";

	module.exports = function createMousePress(opts) {
	  var _ref = opts || {};

	  var _ref$element = _ref.element;
	  var element = _ref$element === undefined ? document : _ref$element;
	  var autoFlush = _ref.autoFlush;
	  var enableRightButton = _ref.enableRightButton;

	  return function (sink) {

	    var state = {
	      leftButton: false,
	      middleButton: false,
	      rightButton: false
	    };

	    sink(state);

	    function onMouseUp(e) {
	      e.preventDefault();

	      state.leftButton = false;
	      state.middleButton = false;
	      state.rightButton = false;

	      sink(state, autoFlush);
	    }

	    function onMouseDown(e) {
	      e.preventDefault();

	      switch (e.button) {
	        case 0:
	          state.leftButton = true;break;
	        case 1:
	          state.middleButton = true;break;
	        case 2:
	          state.rightButton = true;
	      }

	      sink(state, autoFlush);
	    }

	    element.addEventListener("mousedown", onMouseDown);
	    document.addEventListener("mouseup", onMouseUp);

	    if (enableRightButton) {
	      element.addEventListener("contextmenu", function (e) {
	        return e.preventDefault();
	      });
	    }
	  };
	};

/***/ },
/* 74 */
/***/ function(module, exports) {

	"use strict";

	module.exports = function createAnimationFrame(opts) {
	  var _ref = opts || {};

	  var autoFlush = _ref.autoFlush;

	  return function (sink) {

	    var isPlaying = true,
	        oldTime = Date.now(),
	        newTime = null;

	    function next() {
	      if (isPlaying) {
	        newTime = Date.now();
	        sink(newTime - oldTime, autoFlush);
	        oldTime = newTime;
	        requestAnimationFrame(next);
	      }
	    }

	    next();

	    return function stop() {
	      isPlaying = false;
	    };
	  };
	};

/***/ }
/******/ ])
});
;