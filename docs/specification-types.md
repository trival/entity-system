Entity System Data Format
=========================

Data Specification that represents an entity system.
Consists of pure JSON data.


Specification Metainformation
-----------------------------

* DefaultLanguage
    * the default code language in the spec
    * defaults to Javascript



EntitySpec
----------

* Id: [namespace / ] name
    * required.
    * can be generated from namspace and name, separated by "/".

* Name: string
    * required.
    * can be generated from id.
    * The name of the Entity.

* Namespace: string
    * optional.
    * can be generated from id.
    * Group in which entity names must be unique.
    * Each namespace is unique.
    * Usefull for grouping entities.

* InitialValue: JSON
    * optional.
    * The initial value to be set on this entity.
    * Can be any valid JSON.

* ValueType: string
    * optional.
    * defaults to "JSON".
    * type identifier of the value type.
    * helpfull metainformation for editors and viewers.

Read-only computed metadata

* Factory: fid
    * optional.
    * Id of the Factory.

* Reactions: [ rids... ]
    * optional.
    * Ids of the Reactions that manipulate this entity.



FactorySpec
-----------

* Id: ID
    * required.
    * is generated from receiver id if not set explicitly.

* Receiver: eid
    * required.
    * The entity who's value is generated.

* Code: string
    * the code of the procedure

* Language: option
    * default: Javascript
    * the language the procedure code is written in

* Procedure: fn (Dependency values) -> new Receiver value
    * required.
    * The factory operation.

* Dependencies: [eids...]
    * optional.
    * The entities who's values are required for the value generation.



ReactionSpec
------------

* Id: ID
    * required.
    * is generated from receiver and triggers ids if not set explicitly.

* Receiver: eid
    * required.
    * The entity that is reacting.

* Code: string
    * the code of the procedure.

* Language: option
    * default: Javascript
    * the language the procedure code is written in.

* Procedure: fn (Dependency values) -> new Receiver value | void | null
    * required.
    * The reaction operation.

* Triggers: [eids...]
    * required.
    * Entity IDs that cause the reaction to be executed.

* Supplements: [eids...]
    * optional.
    * Entity IDs that are used by this reaction but don't trigger it.



